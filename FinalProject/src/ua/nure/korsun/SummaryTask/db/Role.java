package ua.nure.korsun.SummaryTask.db;

import ua.nure.korsun.SummaryTask.db.entity.User;

/**
 * Role entity.
 * 
 * @author V.Korsun
 * 
 */

public enum Role {
	ADMIN, MANAGER, CLIENT, BLOCK;
	
	public static Role getRole(User user) {
		int roleId = user.getRoleId();
		return Role.values()[roleId];
	}
	
	public String getName() {
		return name().toLowerCase();
	}
	
}