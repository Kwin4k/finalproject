package ua.nure.korsun.SummaryTask.db.bean;

import ua.nure.korsun.SummaryTask.db.entity.Entity;

/**
 * Provide records for virtual table:
 * <pre>
 * |order.id|user.user_id|user.firstName|user.lastName|car.name|order.bill|status.name|
 * </pre>
 * 
 * @author V.Korsun
 * 
 */
public class ClientOrderBean extends Entity {
	
	private static final long serialVersionUID = -5654982557199337483L;

	private long orderId;
	
	private int clientId;
	
	private String clientFirstName;

	private String clientLastName;
	
	private String clientCar;

	private int orderBill;
	
	private String statusName;
	

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientCar() {
		return clientCar;
	}

	public void setClientCar(String clientCar) {
		this.clientCar = clientCar;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getClientFirstName() {
		return clientFirstName;
	}

	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}

	public String getClientLastName() {
		return clientLastName;
	}

	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}

	public int getOrderBill() {
		return orderBill;
	}

	public void setOrderBill(int orderBill) {
		this.orderBill = orderBill;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public String toString() {
		return "OrderClientBean [orderId=" + orderId 
				+ ", clientId="+ clientId 
				+ ", userFirstName="+ clientFirstName 
				+ ", userLastName=" + clientLastName
				+ ", clientCar="+ clientCar
				+ ", orderBill=" + orderBill 
				+ ", statusName=" + statusName + "]";
	}
}
