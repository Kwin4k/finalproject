package ua.nure.korsun.SummaryTask.db.bean;

import ua.nure.korsun.SummaryTask.db.entity.Entity;

/**
 * Provide records for virtual table:
 * <pre>
 * |order.id|car.name|order.bill|status.name|
 * </pre>
 * 
 * @author V.Korsun
 * 
 */
public class ClientOrderBasketBean extends Entity{
	
	private static final long serialVersionUID = -5654982557199337483L;

	private long orderId;
	
	private String carName;

	private int orderBill;

	private String statusName;
	
	
	public void setCarName(String carName) {
		this.carName = carName;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getCarName() {
		return carName;
	}


	public int getOrderBill() {
		return orderBill;
	}

	public void setOrderBill(int orderBill) {
		this.orderBill = orderBill;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	@Override
	public String toString() {
		return "OrderClientBasketBean [orderId=" + orderId 
				+ ", carName="+ carName 
				+ ", orderBill=" + orderBill 
				+ ", statusName=" + statusName + "]";
	}
}
