package ua.nure.korsun.SummaryTask.db;

import ua.nure.korsun.SummaryTask.db.entity.Order;

/**
 * Status entity.
 * 
 * @author V.Korsun
 * 
 */
public enum Status {
	OPENED, BLOCKED, PAID, USE, CLOSED, REPAIR;
	
	public static Status getStatus(Order order) {
		int statusId = order.getStatusId();
		return Status.values()[statusId];
	}
	
	public String getName() {
		return name().toLowerCase();
	}
}