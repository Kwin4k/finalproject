package ua.nure.korsun.SummaryTask.db.entity;

/**
 * Car entity.
 * 
 * @author V.Korsun
 * 
 */
public class Car extends Entity {

	private static final long serialVersionUID = 4716395168539434663L;

	private String name;
	
	private String mark;
	
	private String carClass;
	
	private int price;
	
	private int numberOfUses;

	private int sumForRepair;
	
	public int getNumberOfUses() {
		return numberOfUses;
	}

	public void setNumberOfUses(int numberOfUses) {
		this.numberOfUses = numberOfUses;
	}

	public int getSumForRepair() {
		return sumForRepair;
	}

	public void setSumForRepair(int sumForRepair) {
		this.sumForRepair = sumForRepair;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getCarClass() {
		return carClass;
	}

	public void setCarClass(String carClass) {
		this.carClass = carClass;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [name=" + name 
				+ ", mark=" + mark 
				+ ", carClass=" + carClass 
				+ ", price=" + price 
				+ ", numberOfUses=" + numberOfUses 
				+ ", sumForRepair=" + sumForRepair
				+ ", getId()=" + getId() + "]";
	}
}