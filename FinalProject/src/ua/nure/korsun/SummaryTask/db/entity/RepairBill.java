package ua.nure.korsun.SummaryTask.db.entity;

/**
 * Repair bill entity.
 * 
 * @author V.Korsun
 * 
 */

public class RepairBill extends Entity {

	private static final long serialVersionUID = 5692708766041889396L;
	
	private long orderId;

	private int bill;
	
	private String repairName;

	private String comment;
	
	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getRepairName() {
		return repairName;
	}

	public void setRepairName(String repairName) {
		this.repairName = repairName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	@Override
	public String toString() {
		return "Order [Id=" + getId() 
				+ ", orderId=" + orderId
				+ ", repairName=" + repairName 
				+ ", bill=" + bill 
				+ ", comment" + comment + "]";
	}
}
