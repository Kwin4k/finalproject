package ua.nure.korsun.SummaryTask.db.entity;

/**
 * Service entity.
 * 
 * @author V.Korsun
 * 
 */
public class Service extends Entity {

	private static final long serialVersionUID = 1716385168539434663L;

	private String name;
	
	private int price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Service [name=" + name 
				+ ", price=" + price 
				+ ", getId()=" + getId() + "]";
	}
}