package ua.nure.korsun.SummaryTask.db.entity;

/**
 * Order entity.
 * 
 * @author V.Korsun
 * 
 */
public class Order extends Entity {

	private static final long serialVersionUID = 5692708766041889396L;

	private int bill;

	private int clientId;

	private int carId;

	private String color;

	private int countDays;

	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	private int statusId;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getBill() {
		return bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public int getCountDays() {
		return countDays;
	}

	public void setCountDays(int countDays) {
		this.countDays = countDays;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	@Override
	public String toString() {
		return "Order [clientId=" + clientId
				+ ", carId=" + carId
				+ ", color=" + color 
				+ ", days=" + countDays 
				+ ", bill=" + bill
				+ ", statusId=" + statusId
				+ ", getId()=" + getId() 
				+ ", comment"+ comment + "]";
	}
}
