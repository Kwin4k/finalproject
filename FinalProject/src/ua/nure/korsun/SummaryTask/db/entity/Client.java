package ua.nure.korsun.SummaryTask.db.entity;

/**
 * Client entity.
 * 
 * @author V.Korsun
 * 
 */
public class Client extends Entity {

	private static final long serialVersionUID = 1526302756705518585L;
	
	private int userId;

	private String passportId;
	
	private String email;

	private String phone;

	private String address;

	private int age;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Client [ userId=" + userId
				+ ", passportId=" + passportId
				+ ", address=" + address 
				+ ", email=" + email
				+ ", phone=" + phone
				+ ", age=" + age 
				+ ", getId()=" + getId() + "]";
	}

}
