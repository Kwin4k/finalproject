package ua.nure.korsun.SummaryTask.db.entity;

/**
 * User entity.
 * 
 * @author V.Korsun
 * 
 */
public class User extends Entity {

	private static final long serialVersionUID = -6889036256149495388L;

	private String firstName;

	private String lastName;

	private String login;

	private String password;
	
	private String localeName;

	private int roleId;

	public String getLocaleName() {
		return localeName;
	}

	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId; 
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName 
				+ ", lastName=" + lastName 
				+ ", login=" + login 
				+ ", password=" + password 
				+ ", roleId=" + roleId 
				+ ", localeName=" + localeName
				+ ", Id=" + getId()
				+ "]";
	}

}
