package ua.nure.korsun.SummaryTask.db;

/**
 * Holder for fields names of DB tables and beans.
 * 
 * @author V.Korsun
 * 
 */
public final class Fields {
	
	// entities
	public static final String ENTITY_ID = "id";
	
	public static final String USER_FIRST_NAME = "first_name";
	public static final String USER_LAST_NAME = "last_name";
	public static final String USER_LOGIN = "login";
	public static final String USER_PASSWORD = "password";
	public static final String USER_LOCALE_NAME = "locale_name";
	public static final String USER_ROLE_ID = "role_id";
	
	public static final String CLIENT_PASSPORT_ID= "passport_id";
	public static final String CLIENT_ADDRESS= "address";
	public static final String CLIENT_PHONE = "phone";
	public static final String CLIENT_EMAIL = "email";
	public static final String CLIENT_AGE= "age";
	public static final String CLIENT_USER_ID= "user_id";

	public static final String REPAIR_BILL_ORDER_ID= "repair_id";
	public static final String REPAIR_BILL_REPAIR_NAME= "repair_name";
	public static final String REPAIR_BILL_REPAIR_PRICE= "repair_price";
	public static final String REPAIR_BILL_COMMENT= "comment";
	
	public static final String ORDER_BILL = "bill";
	public static final String ORDER_CLIENT_ID = "client_id";
	public static final String ORDER_STATUS_ID= "status_id";
	public static final String ORDER_CAR_ID= "car_id";
	public static final String ORDER_COLOR= "color";
	public static final String ORDER_DAYS= "days";
	public static final String ORDER_COMMENT= "comment";
	
	public static final String CAR_NAME = "car_name";
	public static final String CAR_MARK = "mark";
	public static final String CAR_CLASS = "class";	
	public static final String CAR_NUM_OF_USES = "number_of_uses";	
	public static final String CAR_SUM_FOR_REPAIR = "sum_for_repair";	
	public static final String CAR_PRICE = "price";	
	
	public static final String SERVICES_NAME = "name";	
	public static final String SERVICES_PRICE = "price";	
	
	public static final String STATUS_NAME = "name";

	// beans
	public static final String CLIENT_ORDER_BEAN_ORDER_ID = "id";	
	public static final String CLIENT_ORDER_BEAN_CLIENT_ID = "client_id";
	public static final String CLIENT_ORDER_BEAN_CLIENT_FIRST_NAME = "first_name";	
	public static final String CLIENT_ORDER_BEAN_CLIENT_LAST_NAME = "last_name";
	public static final String CLIENT_ORDER_BEAN_CLIENT_CAR_NAME = "car_name";
	public static final String CLIENT_ORDER_BEAN_ORDER_BILL = "bill";	
	public static final String CLIENT_ORDER_BEAN_STATUS_NAME = "statusName";
	
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_ORDER_ID = "repair_id";	
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_CAR_NAME = "car_name";
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_PRICE = "price";	
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_DAYS = "days";
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_STATUS = "status_id";
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_REPAIR_NAME = "repair_name";	
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_REPAIR_PRICE = "repair_price";
	public static final String CLIENT_ORDER_REPAIR_BILL_BEAN_COMMENT = "comment";
	
	public static final String CLIENT_ORDER_BASKET_BEAN_ORDER_ID = "id";	
	public static final String CLIENT_ORDER_BASKET_BEAN_CAR_NAME = "car_name";	
	public static final String CLIENT_ORDER_BASKET_BEAN_ORDER_BILL = "bill";	
	public static final String CLIENT_ORDER_BASKET_BEAN_STATUS_NAME = "statusName";
}