package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.entity.Service;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for service related entities.
 */
public class ServiceDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_GET_ALL_SERVICES = "SELECT * FROM services";

	/**
	 * Find list of services.
	 * 
	 * @return list of services
	 */
	public List<Service> getListServices() throws DBException {
		List<Service> service = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_GET_ALL_SERVICES);
			while (rs.next()) {
				service.add(extractListServices(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_LIST_SERVICES, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_LIST_SERVICES, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return service;
	}

	/**
	 * Extracts a service from the result set row.
	 */
	private Service extractListServices(ResultSet rs) throws SQLException {
		Service service = new Service();
		service.setId(rs.getLong(Fields.ENTITY_ID));
		service.setName(rs.getString(Fields.SERVICES_NAME));
		service.setPrice(rs.getInt(Fields.SERVICES_PRICE));
		return service;
	}
}
