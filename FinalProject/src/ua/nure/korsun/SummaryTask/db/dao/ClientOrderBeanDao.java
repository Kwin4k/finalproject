package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for client order bean related entities.
 */
public class ClientOrderBeanDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_FIND_CLIENT_ORDER_BEAN_BY_STATUS_ID = "SELECT o.id, o.client_id, u.first_name, u.last_name, c.car_name, o.bill, s.statusName"
			+ " FROM users u, orders o, clients cl, statuses s, cars c"
			+ " WHERE o.status_id=s.id AND o.car_id=c.id AND o.client_id = cl.id AND cl.user_id = u.id AND o.status_id=?";

	private static final String SQL_GET_CLIENT_ORDER_BEAN_BY_ORDER_ID = "SELECT o.id, o.client_id, u.first_name, u.last_name, c.car_name, o.bill, s.statusName"
			+ " FROM users u, orders o, clients cl, statuses s, cars c"
			+ " WHERE o.status_id=s.id AND o.car_id=c.id AND o.client_id = cl.id AND cl.user_id = u.id AND o.id=?";

	private static final String SQL_GET_CLIENT_ORDER_BEANS = "SELECT o.id, o.client_id, u.first_name, u.last_name, c.car_name, o.bill, s.statusName"
			+ "	FROM users u, clients cl, orders o, statuses s, cars c"
			+ "	WHERE o.client_id = cl.id AND cl.user_id = u.id AND o.status_id=s.id AND o.car_id=c.id";

	private static final String SQL_GET_ORDERS_BY_CLIENT_ID = "SELECT o.id, o.client_id, u.first_name, u.last_name, c.car_name, o.bill, s.statusName"
			+ "	FROM users u, clients cl, orders o, statuses s, cars c"
			+ "	WHERE o.client_id = cl.id AND cl.user_id = u.id AND o.status_id=s.id AND o.car_id=c.id AND o.client_id=?";

	private static final String SQL_GET_ORDERS_BY_STATUS = "SELECT o.id, o.client_id, u.first_name, u.last_name, c.car_name, o.bill, s.statusName"
			+ "	FROM users u, clients cl, orders o, statuses s, cars c"
			+ "	WHERE o.client_id = cl.id AND cl.user_id = u.id AND o.status_id=s.id AND o.car_id=c.id AND o.status_id=?";

	/**
	 * Find all client order beans.
	 *
	 * @return list of client order beans.
	 */
	public List<ClientOrderBean> getClientOrderBeans() throws DBException {
		List<ClientOrderBean> orderUserBeanList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_GET_CLIENT_ORDER_BEANS);
			while (rs.next()) {
				orderUserBeanList.add(extractClientOrderBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BEAN, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BEAN, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderUserBeanList;
	}

	/**
	 * Find client order basket bean by client id.
	 *
	 * @return client order basket bean.
	 * @param id order.
	 */
	public ClientOrderBean getClientOrderBeanByOrderId(long resultId) throws DBException {
		ClientOrderBean orderBean = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_GET_CLIENT_ORDER_BEAN_BY_ORDER_ID);
			pstmt.setLong(1, resultId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderBean = extractClientOrderBean(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BEAN_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BEAN_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderBean;
	}

	/**
	 * Find client order basket bean by status id.
	 *
	 * @return list of client order basket bean.
	 * @param id status.
	 */
	public List<ClientOrderBean> getClientOrderBeansByStatus(int statusId) throws DBException {
		List<ClientOrderBean> orderUserBeanList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CLIENT_ORDER_BEAN_BY_STATUS_ID);
			pstmt.setInt(1, statusId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderUserBeanList.add(extractClientOrderBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CLIENT_ORDER_BEAN_BY_STATUS_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CLIENT_ORDER_BEAN_BY_STATUS_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderUserBeanList;
	}

	/**
	 * Find list of client order basket bean by status id.
	 *
	 * @return list of client order basket bean.
	 * @param id client.
	 */
	public List<ClientOrderBean> findOrdersSelectByClientId(String selectBy) throws DBException {
		List<ClientOrderBean> orderBean = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_GET_ORDERS_BY_CLIENT_ID);
			pstmt.setString(1, selectBy);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderBean.add(extractClientOrderBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_ORDERS_BY_CLIENT_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_ORDERS_BY_CLIENT_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderBean;
	}

	/**
	 * Find list of client order basket bean by status id.
	 *
	 * @return list of client order basket bean.
	 * @param id status.
	 */
	public List<ClientOrderBean> findOrdersSelectByStatus(String selectBy) throws DBException {
		List<ClientOrderBean> orderBean = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			;
			pstmt = con.prepareStatement(SQL_GET_ORDERS_BY_STATUS);
			pstmt.setString(1, selectBy);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderBean.add(extractClientOrderBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_ORDERS_BY_STATUS, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_ORDERS_BY_STATUS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderBean;
	}

	/**
	 * Extracts a client order bean from the result set row.
	 */
	private ClientOrderBean extractClientOrderBean(ResultSet rs) throws SQLException {
		ClientOrderBean bean = new ClientOrderBean();
		bean.setOrderId(rs.getLong(Fields.CLIENT_ORDER_BEAN_ORDER_ID));
		bean.setClientId(rs.getInt(Fields.CLIENT_ORDER_BEAN_CLIENT_ID));
		bean.setOrderBill(rs.getInt(Fields.CLIENT_ORDER_BEAN_ORDER_BILL));
		bean.setClientCar(rs.getString(Fields.CLIENT_ORDER_BEAN_CLIENT_CAR_NAME));
		bean.setClientFirstName(rs.getString(Fields.CLIENT_ORDER_BEAN_CLIENT_FIRST_NAME));
		bean.setClientLastName(rs.getString(Fields.CLIENT_ORDER_BEAN_CLIENT_LAST_NAME));
		bean.setStatusName(rs.getString(Fields.CLIENT_ORDER_BEAN_STATUS_NAME));
		return bean;
	}
}
