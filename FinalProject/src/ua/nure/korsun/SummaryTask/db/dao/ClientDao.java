package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.entity.Client;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for client related entities.
 */
public class ClientDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_FIND_CLIENT_BY_USER_ID = "SELECT * FROM clients WHERE user_id=?";

	private static final String SQL_FIND_CLIENT_BY_ID = "SELECT * FROM clients WHERE id=?";

	private static final String SQL_UPDATE_CLIENT = "UPDATE clients SET passport_id=?, age=?, address=?, phone=?, email=? WHERE id=?";

	private static final String SQL_ADD_USER_ID_TO_CLIENT = "INSERT INTO clients VALUES(DEFAULT, ?, NULL, NULL, NULL, NULL, NULL);";

	/**
	 * Find client by user Id.
	 *
	 * @return client with search id.
	 * @param id user.
	 */
	public Client findClientByUserId(long userId) throws DBException {
		Client client = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CLIENT_BY_USER_ID);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				client = extractClient(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CLIENT, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CLIENT, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return client;
	}

	/**
	 * Find client by Id.
	 *
	 * @return client with search id.
	 * @param id client.
	 */
	public Client findClientById(long clientId) throws DBException {
		Client client = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CLIENT_BY_ID);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				client = extractClient(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CLIENT, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CLIENT, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return client;
	}

	/**
	 * Add user id to client.
	 *
	 * @param id user.
	 */
	public void addUserId2Client(long id) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_USER_ID_TO_CLIENT);
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_USER_ID_TO_CLIENT, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_USER_ID_TO_CLIENT, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Update client.
	 *
	 * @param client for update.
	 */
	public void updateClient(Client client) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_CLIENT);
			int k = 1;
			pstmt.setString(k++, client.getPassportId());
			pstmt.setInt(k++, client.getAge());
			pstmt.setString(k++, client.getAddress());
			pstmt.setString(k++, client.getPhone());
			pstmt.setString(k++, client.getEmail());
			pstmt.setLong(k, client.getId());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_UPDATE_CLIENT, ex);
			throw new DBException(Messages.ERR_CANNOT_UPDATE_CLIENT, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Extracts a client from the result set row.
	 */
	private Client extractClient(ResultSet rs) throws SQLException {
		Client client = new Client();
		client.setId(rs.getLong(Fields.ENTITY_ID));
		client.setUserId(rs.getInt(Fields.CLIENT_USER_ID));
		client.setPassportId(rs.getString(Fields.CLIENT_PASSPORT_ID));
		client.setAddress(rs.getString(Fields.CLIENT_ADDRESS));
		client.setPhone(rs.getString(Fields.CLIENT_PHONE));
		client.setEmail(rs.getString(Fields.CLIENT_EMAIL));
		client.setAge(rs.getInt(Fields.CLIENT_AGE));
		return client;
	}
}
