package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for order related entities.
 */ 
public class OrderDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_GET_LIST_CLIENT_ID = "SELECT DISTINCT client_id FROM orders";

	private static final String SQL_GET_LIST_STATUS_ORDER = "SELECT DISTINCT status_id FROM orders";

	private static final String SQL_ADD_ORDER = "INSERT INTO orders VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, NULL)";

	private static final String SQL_FIND_ORDERS_BY_USER_ID = "SELECT * FROM orders WHERE client_id=?";
	
	private static final String SQL_FIND_ORDERS_BY_ORDER_ID = "SELECT * FROM orders WHERE id=?";

	private static final String SQL_UPDATE_CLIENT_ORDER = "UPDATE orders SET status_id=? WHERE id=?";

	private static final String SQL_DELETE_ORDER_BY_ID = "DELETE FROM orders WHERE id = ?";

	private static final String SQL_UPDATE_ORDER_STATUS = "UPDATE orders SET status_id=?, comment=? WHERE id=?";

	/**
	 * Find list of client orders by client id.
	 *
	 * @return list of client orders.
	 * @param id client.
	 */
	public List<Order> findClientOrders(long clientId) throws DBException {
		List<Order> clientOrdersList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ORDERS_BY_USER_ID);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				clientOrdersList.add(extractOrder(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_ORDER, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_ORDER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return clientOrdersList;
	}
	
	public Order findOrderById(long orderId) throws DBException {
		Order order = new Order();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ORDERS_BY_ORDER_ID);
			pstmt.setLong(1, orderId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				order = extractOrder(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_ORDER, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_ORDER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return order;
	}

	/**
	 * Delete order by id.
	 *
	 * @param id order.
	 */
	public void deleteOrder(long orderId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_ORDER_BY_ID);
			pstmt.setLong(1, orderId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_DELETE_ORDER, ex);
			throw new DBException(Messages.ERR_CANNOT_DELETE_ORDER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Add new order.
	 *
	 * @param order for add.
	 */
	public void addClientOrder(Order order) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_ORDER);
			int k = 1;
			pstmt.setInt(k++, order.getClientId());
			pstmt.setInt(k++, order.getCarId());
			pstmt.setString(k++, order.getColor());
			pstmt.setInt(k++, order.getCountDays());
			pstmt.setInt(k++, order.getBill());
			pstmt.setInt(k, order.getStatusId());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_ORDER, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_ORDER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Update order by id.
	 * 
	 * @param id order.
	 */
	public void updateClientOrder(int id) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_CLIENT_ORDER);
			int k = 1;
			pstmt.setInt(k++, 2);
			pstmt.setInt(k++, id);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_UPDATE_ORDER, ex);
			throw new DBException(Messages.ERR_CANNOT_UPDATE_ORDER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Update status order by id.
	 * 
	 * @param id order, order for update.
	 */
	public void updateOrderStatusByOrderId(Order order, long orderId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_ORDER_STATUS);
			int k = 1;
			pstmt.setInt(k++, order.getStatusId());
			if (order.getComment() != null) {
				pstmt.setString(k++, order.getComment());
			} else {
				pstmt.setString(k++, null);
			}
			pstmt.setLong(k, orderId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_UPDATE_ORDER_STATUS, ex);
			throw new DBException(Messages.ERR_CANNOT_UPDATE_ORDER_STATUS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Find list of statuses.
	 * 
	 * @return list of statuses.
	 */
	public List<Order> getListStatusOrder() throws DBException {
		List<Order> listStatus = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_GET_LIST_STATUS_ORDER);
			while (rs.next()) {
				listStatus.add(extractListStatusOrder(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_LIST_STATUS, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_LIST_STATUS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return listStatus;
	}

	/**
	 * Find list of client id.
	 * 
	 * @return list of client id.
	 */
	public List<Order> getListClientId() throws DBException {
		List<Order> listClientId = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_GET_LIST_CLIENT_ID);
			while (rs.next()) {
				listClientId.add(extractListClientId(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_LIST_CLIENT_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_LIST_CLIENT_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return listClientId;
	}

	/**
	 * Extracts a order from the result set row.
	 */
	private Order extractOrder(ResultSet rs) throws SQLException {
		Order order = new Order();
		order.setId(rs.getLong(Fields.ENTITY_ID));
		order.setClientId(rs.getInt(Fields.ORDER_CLIENT_ID));
		order.setCarId(rs.getInt(Fields.ORDER_CAR_ID));
		order.setColor(rs.getString(Fields.ORDER_COLOR));
		order.setCountDays(rs.getInt(Fields.ORDER_DAYS));
		order.setBill(rs.getInt(Fields.ORDER_BILL));
		order.setStatusId(rs.getInt(Fields.ORDER_STATUS_ID));
		order.setComment(rs.getString(Fields.ORDER_COMMENT));
		return order;
	}

	/**
	 * Extracts a status order from the result set row.
	 */
	private Order extractListStatusOrder(ResultSet rs) throws SQLException {
		Order order = new Order();
		order.setStatusId(rs.getInt(Fields.ORDER_STATUS_ID));
		return order;
	}

	/**
	 * Extracts a client id from the result set row.
	 */
	private Order extractListClientId(ResultSet rs) throws SQLException {
		Order order = new Order();
		order.setClientId(rs.getInt(Fields.ORDER_CLIENT_ID));
		return order;
	}

}
