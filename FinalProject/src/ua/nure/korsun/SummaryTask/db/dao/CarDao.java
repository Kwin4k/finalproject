package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for car related entities.
 */
public class CarDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_FIND_ALL_CAR_ITEMS = "SELECT * FROM cars";

	private static final String SQL_FIND_ALL_CARS_CLASS = "SELECT DISTINCT class FROM cars";

	private static final String SQL_FIND_ALL_CARS_MARK = "SELECT DISTINCT mark FROM cars";

	private static final String SQL_FIND_CARS_BY_CLASS = "SELECT * FROM cars WHERE class=?";

	private static final String SQL_FIND_CARS_BY_MARK = "SELECT * FROM cars WHERE mark=?";

	private static final String SQL_FIND_CAR_BY_ID = "SELECT * FROM cars WHERE id=?";

	private static final String SQL_CANGE_CAR_BY_ID = "UPDATE cars SET car_name=?, mark=?, class=?, price=? WHERE id=?";
	
	private static final String SQL_UPDATE_NUM_CAR_BY_ID = "UPDATE cars SET number_of_uses=? WHERE id=?";
	
	private static final String SQL_UPDATE_SUM_CAR_BY_ID = "UPDATE cars SET sum_for_repair=? WHERE id=?";

	private static final String SQL_ADD_NEW_CAR = "INSERT INTO cars VALUES(DEFAULT, ?, ?, ?, ?)";

	private static final String SQL_DELETE_CAR_BY_ID = "DELETE FROM cars WHERE id = ?";

	/**
	 * Change car by car id.
	 *
	 * @param car to change.
	 */
	public void changeCarById(Car car) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_CANGE_CAR_BY_ID);
			int k = 1;
			pstmt.setString(k++, car.getName());
			pstmt.setString(k++, car.getMark());
			pstmt.setString(k++, car.getCarClass());
			pstmt.setInt(k++, car.getPrice());
			pstmt.setLong(k, car.getId());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Find car by parameter.
	 *
	 * @return List of car item entities.
	 * @param String select by class.
	 */
	public List<Car> findCarsBySelectClass(String selectBy) throws DBException {
		List<Car> cars = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CARS_BY_CLASS);
			pstmt.setString(1, selectBy);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cars.add(extractCar(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CARS, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CARS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return cars;
	}

	/**
	 * Find all car classes.
	 *
	 * @return List of car classes.
	 */
	public List<Car> findCarsClass() throws DBException {
		List<Car> cars = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_CARS_CLASS);
			while (rs.next()) {
				cars.add(extractCarsClass(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CARS_CLASS, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CARS_CLASS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return cars;
	}

	/**
	 * Find all car marks.
	 *
	 * @return List of car marks.
	 */
	public List<Car> findCarsMark() throws DBException {
		List<Car> cars = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_CARS_MARK);
			while (rs.next()) {
				cars.add(extractCarsMark(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CAR_MARK, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CAR_MARK, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}

		return cars;
	}

	/**
	 * Find car by select mark.
	 *
	 * @return List of car item entities with select mark.
	 * @param String mark
	 */
	public List<Car> findCarsBySelectMark(String selectBy) throws DBException {
		List<Car> cars = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CARS_BY_MARK);
			pstmt.setString(1, selectBy);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cars.add(extractCar(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CAR_BY_MARK, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CAR_BY_MARK, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return cars;
	}

	/**
	 * Find car items.
	 *
	 * @return List of car items.
	 */
	public List<Car> findCarItems() throws DBException {
		List<Car> carItemsList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_CAR_ITEMS);
			LOG.error(rs);
			while (rs.next()) {
				carItemsList.add(extractCar(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CAR_ITEMS, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CAR_ITEMS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return carItemsList;
	}

	/**
	 * Find car by id.
	 *
	 * @return car by id sought.
	 * @param car id.
	 */
	public Car findCarById(int carId) throws DBException {
		Car car = new Car();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_CAR_BY_ID);
			pstmt.setLong(1, carId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				car = extractCar(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_CAR_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_CAR_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return car;
	}

	/**
	 * Add new Car.
	 *
	 * @param car to add.
	 */
	public void addNewCar(Car car) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_NEW_CAR);
			int k = 1;
			pstmt.setString(k++, car.getName());
			pstmt.setString(k++, car.getMark());
			pstmt.setString(k++, car.getCarClass());
			pstmt.setInt(k++, car.getPrice());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_NEW_CAR, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_NEW_CAR, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
	
	public void updateCarNumOfUsesById(int carId, int numberOfUses) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_NUM_CAR_BY_ID);
			int k = 1;
			pstmt.setLong(k++, numberOfUses);
			pstmt.setLong(k, carId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
	
	public void updateCarSumForRepairById(int carId, int sumForRepair) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_SUM_CAR_BY_ID);
			int k = 1;
			pstmt.setLong(k++, sumForRepair);
			pstmt.setLong(k, carId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_CHANGE_CAR_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Delete Car by id.
	 *
	 * @param id delete car.
	 */
	public void deleteCarById(long resultId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_CAR_BY_ID);
			pstmt.setLong(1, resultId);
			pstmt.executeUpdate();
			con.commit(); 
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_DELETE_CAR_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_DELETE_CAR_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
     * Extracts a car from the result set row.
     */
	private Car extractCar(ResultSet rs) throws SQLException {
		Car car = new Car();
		car.setId(rs.getLong(Fields.ENTITY_ID));
		car.setName(rs.getString(Fields.CAR_NAME));
		car.setMark(rs.getString(Fields.CAR_MARK));
		car.setCarClass(rs.getString(Fields.CAR_CLASS));
		car.setNumberOfUses(rs.getInt(Fields.CAR_NUM_OF_USES));
		car.setSumForRepair(rs.getInt(Fields.CAR_SUM_FOR_REPAIR));
		car.setPrice(rs.getInt(Fields.CAR_PRICE));
		return car;
	}
	
	/**
     * Extracts a class from the result set row.
     */
	private Car extractCarsClass(ResultSet rs) throws SQLException {
		Car car = new Car();
		car.setCarClass(rs.getString(Fields.CAR_CLASS));
		return car;
	}
	
	/**
     * Extracts a mark from the result set row.
     */
	private Car extractCarsMark(ResultSet rs) throws SQLException {
		Car car = new Car();
		car.setMark(rs.getString(Fields.CAR_MARK));
		return car;
	}

}
