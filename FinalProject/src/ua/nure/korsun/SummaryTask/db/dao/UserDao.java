package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for user related entities.
 */
public class UserDao {
	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";

	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";

	private static final String SQL_FIND_USER_BY_ID = "SELECT * FROM users WHERE id=?";

	private static final String SQL_FIND_MANAGERS = "SELECT * FROM users WHERE role_id=?";

	private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";

	private static final String SQL_UPDATE_USER_ROLE = "UPDATE users SET role_id=? WHERE id=?";

	private static final String SQL_ADD_USER = "INSERT INTO users VALUES(DEFAULT, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_UPDATE_USER = "UPDATE users SET  first_name=?, last_name=?, locale_name=?, password=?"
			+ " WHERE id=?";

	/**
	 * Update user.
	 * 
	 * @param user for update.
	 */
	public void updateUser(User user) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_USER);
			int k = 1;
			pstmt.setString(k++, user.getFirstName());
			pstmt.setString(k++, user.getLastName());
			pstmt.setString(k++, user.getLocaleName());
			pstmt.setString(k++, user.getPassword());
			pstmt.setLong(k, user.getId());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_UPDATE_USER, ex);
			throw new DBException(Messages.ERR_CANNOT_UPDATE_USER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Find list of users.
	 * 
	 * @return list of users.
	 */
	public List<User> findUsers() throws DBException {
		List<User> usersList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				usersList.add(extractUser(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_USER, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_USER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return usersList;
	}

	/**
	 * Add new user.
	 * 
	 * @param user for add.
	 * @throws DBException
	 */
	public void addUser(User user) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_USER);
			int k = 1;
			pstmt.setString(k++, user.getFirstName());
			pstmt.setString(k++, user.getLastName());
			pstmt.setString(k++, user.getLogin());
			pstmt.setString(k++, user.getPassword());
			pstmt.setString(k++, user.getLocaleName());
			pstmt.setInt(k, user.getRoleId());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_USER, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_USER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Returns a user with the given login.
	 * 
	 * @param login User login.
	 * @return User entity.
	 */
	public User findUserByLogin(String login) throws DBException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_USER_BY_LOGIN, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_USER_BY_LOGIN, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return user;
	}

	/**
	 * Find user with id.
	 * 
	 * @param userId user id.
	 * @return User entity.
	 */
	public User findUserById(long userId) throws DBException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_ID);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_USER_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_USER_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return user;
	}

	/**
	 * Delete user by id.
	 * 
	 * @param userId user id.
	 * @throws DBException
	 */
	public void deleteUser(long userId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USER_BY_ID);
			pstmt.setLong(1, userId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_DELETE_USER_BY_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_DELETE_USER_BY_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Find manager.
	 * 
	 * @return List of managers.
	 */
	public List<User> findManagers() throws DBException {
		List<User> managers = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_FIND_MANAGERS);
			pstmt.setLong(1, 1);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				managers.add(extractUser(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_FIND_MANAGER, ex);
			throw new DBException(Messages.ERR_CANNOT_FIND_MANAGER, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return managers;
	}

	/**
	 * Update user role by user id.
	 * 
	 * @param userId user id, roleId role id.
	 * @return User entity.
	 */
	public void updateUserRole(long userId, int roleId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_USER_ROLE);
			pstmt.setLong(1, roleId);
			pstmt.setLong(2, userId);
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_UPDATE_USER_ROLE, ex);
			throw new DBException(Messages.ERR_CANNOT_UPDATE_USER_ROLE, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Extracts a user from the result set row.
	 */
	private User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getLong(Fields.ENTITY_ID));
		user.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
		user.setLastName(rs.getString(Fields.USER_LAST_NAME));
		user.setLogin(rs.getString(Fields.USER_LOGIN));
		user.setPassword(rs.getString(Fields.USER_PASSWORD));
		user.setLocaleName(rs.getString(Fields.USER_LOCALE_NAME));
		user.setRoleId(rs.getInt(Fields.USER_ROLE_ID));
		return user;
	}
}
