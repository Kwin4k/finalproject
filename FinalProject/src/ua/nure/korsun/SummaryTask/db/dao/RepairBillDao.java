package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.entity.RepairBill;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for repair bill related entities.
 */
public class RepairBillDao {
	
	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_ADD_NEW_REPAIR_BILL = "INSERT INTO repair_bill VALUES(DEFAULT, ?, ?, ?, ?)";
	
	/**
	 * Add repair bill.
	 * 
	 * @param repair bill, id order.
	 */
	public void addRepairBill(RepairBill repairBill, long orderId) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_NEW_REPAIR_BILL);
			int k = 1;
			pstmt.setString(k++, repairBill.getRepairName());
			pstmt.setInt(k++, repairBill.getBill());
			pstmt.setString(k, repairBill.getComment());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_REPAIR_BILL_BY_ORDER_ID, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_REPAIR_BILL_BY_ORDER_ID, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	/**
	 * Add repair bill to order.
	 * 
	 * @param repair bill.
	 */
	public void addRepairBillToOrder(RepairBill repairBill) throws DBException {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_ADD_NEW_REPAIR_BILL);
			int k = 1;
			pstmt.setLong(k++, repairBill.getOrderId());
			pstmt.setString(k++, repairBill.getRepairName());
			pstmt.setInt(k++, repairBill.getBill());
			pstmt.setString(k, repairBill.getComment());
			pstmt.executeUpdate();
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_ADD_REPAIR_BILL, ex);
			throw new DBException(Messages.ERR_CANNOT_ADD_REPAIR_BILL, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
}
