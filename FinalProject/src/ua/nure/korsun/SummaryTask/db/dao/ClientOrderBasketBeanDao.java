package ua.nure.korsun.SummaryTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.db.DBManager;
import ua.nure.korsun.SummaryTask.db.Fields;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBasketBean;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

/**
 * Data access object for client order basket bean related entities.
 */
public class ClientOrderBasketBeanDao {

	private static final Logger LOG = Logger.getLogger(DBManager.class);

	private static final String SQL_GET_CLIENT_ORDER_BASKET_BEANS = "SELECT o.id, c.car_name, o.bill, s.statusName"
			+ "	FROM cars c, orders o, statuses s"
			+ "	WHERE o.car_id=c.id AND o.status_id=s.id AND o.client_id=? AND status_id NOT IN (1,3,4,5)";

	private static final String SQL_GET_CLIENT_ORDER_BASKET_BEANS_REPAIR = "SELECT o.id, c.car_name, o.bill, s.statusName"
			+ "	FROM cars c, orders o, statuses s"
			+ "	WHERE o.car_id=c.id AND o.status_id=s.id AND o.client_id=? AND status_id NOT IN (0,2,3,4)";

	/**
	 * Find client basket bean by client id.
	 *
	 * @return list of client basket order.
	 * @param id client.
	 */
	public List<ClientOrderBasketBean> getClientOrderBasketBeans(long clientId) throws DBException {
		List<ClientOrderBasketBean> orderUserBeanBasketList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_GET_CLIENT_ORDER_BASKET_BEANS);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderUserBeanBasketList.add(extractClientOrderBasketBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderUserBeanBasketList;
	}

	/**
	 * Find client basket bean for repair by client id.
	 *
	 * @return list of client basket order for repair.
	 * @param id client.
	 */
	public List<ClientOrderBasketBean> getClientOrderBasketBeansRepair(long clientId) throws DBException {
		List<ClientOrderBasketBean> orderUserBeanBasketList = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL_GET_CLIENT_ORDER_BASKET_BEANS_REPAIR);
			pstmt.setLong(1, clientId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				orderUserBeanBasketList.add(extractClientOrderBasketBean(rs));
			}
			con.commit();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			LOG.error(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS_REPAIR, ex);
			throw new DBException(Messages.ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS_REPAIR, ex);
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return orderUserBeanBasketList;
	}

	/**
	 * Extracts a client order basket bean from the result set row.
	 */
	private ClientOrderBasketBean extractClientOrderBasketBean(ResultSet rs) throws SQLException {
		ClientOrderBasketBean basketBean = new ClientOrderBasketBean();
		basketBean.setOrderId(rs.getLong(Fields.CLIENT_ORDER_BASKET_BEAN_ORDER_ID));
		basketBean.setOrderBill(rs.getInt(Fields.CLIENT_ORDER_BASKET_BEAN_ORDER_BILL));
		basketBean.setCarName(rs.getString(Fields.CLIENT_ORDER_BASKET_BEAN_CAR_NAME));
		basketBean.setStatusName(rs.getString(Fields.CLIENT_ORDER_BASKET_BEAN_STATUS_NAME));
		return basketBean;
	}

}
