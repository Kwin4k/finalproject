package ua.nure.korsun.SummaryTask.exception;

/**
 * Holder for messages of exceptions.
 * 
 * @author V.Korsun
 *
 */
public class Messages {

	private Messages() {
		// no op
	}
	

	public static final String ERR_CANNOT_OBTAIN_CONNECTION = "Cannot obtain a connection from the pool";	

	public static final String ERR_CANNOT_CLOSE_CONNECTION = "Cannot close a connection";

	public static final String ERR_CANNOT_CLOSE_RESULTSET = "Cannot close a result set";

	public static final String ERR_CANNOT_CLOSE_STATEMENT = "Cannot close a statement";

	public static final String ERR_CANNOT_OBTAIN_DATA_SOURCE = "Cannot obtain the data source";
	
	

	public static final String ERR_CANNOT_CHANGE_CAR_BY_ID = "Cannot change car by car id";

	public static final String ERR_CANNOT_FIND_CARS = "Cannot find cars";

	public static final String ERR_CANNOT_FIND_CARS_CLASS = "Cannot find car classes";

	public static final String ERR_CANNOT_FIND_CAR_MARK = "Cannot find car marks";

	public static final String ERR_CANNOT_FIND_CAR_BY_MARK = "Cannot find car by car mark";

	public static final String ERR_CANNOT_FIND_CAR_ITEMS = "Cannot find cars";

	public static final String ERR_CANNOT_FIND_CAR_BY_ID = "Cannot find car by car id";

	public static final String ERR_CANNOT_ADD_NEW_CAR = "Cannot add new car";

	public static final String ERR_CANNOT_DELETE_CAR_BY_ID = "Cannot delete car by car id ";
	
	

	public static final String ERR_CANNOT_UPDATE_CLIENT = "Cannot update client";

	public static final String ERR_CANNOT_FIND_CLIENT = "Cannot find client";

	public static final String ERR_CANNOT_ADD_USER_ID_TO_CLIENT = "Cannot add user id to client";
	
	

	public static final String ERR_CANNOT_GET_LIST_CLIENT_ID = "Cannot get list client id";

	public static final String ERR_CANNOT_GET_LIST_STATUS = "Cannot get list statuses";

	public static final String ERR_CANNOT_UPDATE_ORDER_STATUS = "Cannot update status in order";

	public static final String ERR_CANNOT_UPDATE_ORDER = "Cannot update order";

	public static final String ERR_CANNOT_ADD_ORDER = "Cannot add new order";

	public static final String ERR_CANNOT_DELETE_ORDER = "Cannot delete order";

	public static final String ERR_CANNOT_FIND_ORDER = "Cannot find order";
	
	

	public static final String ERR_CANNOT_FIND_USER = "Cannot find user";
	
	public static final String ERR_CANNOT_UPDATE_USER = "Cannot update user";

	public static final String ERR_CANNOT_ADD_USER = "Cannot add user";

	public static final String ERR_CANNOT_FIND_USER_BY_LOGIN = "Cannot find user by login";

	public static final String ERR_CANNOT_FIND_USER_BY_ID = "Cannot find user by id";

	public static final String ERR_CANNOT_FIND_MANAGER = "Cannot find manager";

	public static final String ERR_CANNOT_UPDATE_USER_ROLE = "Cannot update user role";
	
	public static final String ERR_CANNOT_DELETE_USER_BY_ID = "Cannot delete user by id";
	
	

	public static final String ERR_CANNOT_GET_LIST_SERVICES = "Cannot get list of services";
	
	

	public static final String ERR_CANNOT_ADD_REPAIR_BILL = "Cannot add bill for repair";

	public static final String ERR_CANNOT_ADD_REPAIR_BILL_BY_ORDER_ID = "Cannot add bill for repair by order id";
	
	

	public static final String ERR_CANNOT_GET_CLIENT_ORDER_BEAN = "Cannot get client order bean";

	public static final String ERR_CANNOT_GET_CLIENT_ORDER_BEAN_BY_ID = "Cannot get client order bean by id";

	public static final String ERR_CANNOT_FIND_CLIENT_ORDER_BEAN_BY_STATUS_ID = "Cannot get client order bean by status id";

	public static final String ERR_CANNOT_GET_ORDERS_BY_CLIENT_ID = "Cannot get orders by client id";

	public static final String ERR_CANNOT_GET_ORDERS_BY_STATUS = "Cannot get orders by status";
	
	

	public static final String ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS = "Cannot get client order basket beans";

	public static final String ERR_CANNOT_GET_CLIENT_ORDER_BASKET_BEANS_REPAIR = "Cannot get client order basket beans repair";
	

	public static final String ERR_PASSWORD_MISMATCH = "Password mismatch";

}