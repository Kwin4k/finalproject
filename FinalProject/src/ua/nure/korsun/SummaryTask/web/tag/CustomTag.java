package ua.nure.korsun.SummaryTask.web.tag;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Custom tag for show data.
 * 
 * @author V.Korsun
 * 
 */
public class CustomTag extends TagSupport {

	private static final long serialVersionUID = 6018377518694252000L;

	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		
		try {
			out.println(dateFormat.format(new Date()));
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return SKIP_BODY;
	}
}