package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Block user command.
 * 
 * @author V.Korsun
 * 
 */
public class BlockUserCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger LOG = Logger.getLogger(BlockUserCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		if (request.getParameter("userId") != null) {
			long userId = Long.parseLong(request.getParameter("userId"));
			LOG.trace("userId --> " + userId);

			new UserDao().updateUserRole(userId, 3);
			LOG.trace("Block user by Id -->" + userId);
		} 
		
		if (request.getParameter("userIdUnBlock") != null) {
			long userId = Long.parseLong(request.getParameter("userIdUnBlock"));
			LOG.trace("userId --> " + userId);

			new UserDao().updateUserRole(userId, 2);
			LOG.trace("UnBlock user by Id -->" + userId);
		}
		
		if (request.getParameter("deleteUser") != null) {
			
			long userId = Long.parseLong(request.getParameter("deleteUser"));

			LOG.trace("Request parameter: user with id --> " + userId);
 
			new UserDao().deleteUser(userId);
			LOG.trace("Delete user");
		}

		LOG.debug("Command finished");
		return Path.COMMAND_LIST_USERS;
	}

}