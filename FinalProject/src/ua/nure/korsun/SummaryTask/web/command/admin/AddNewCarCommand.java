package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Add new car command.
 * 
 * @author V.Korsun
 * 
 */
public class AddNewCarCommand extends Command {

	private static final long serialVersionUID = 1873972253189581503L;

	private static final Logger LOG = Logger.getLogger(AddNewCarCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");

		Car car = new Car();

		String nameCar = request.getParameter("nameCar");
		car.setName(nameCar);
		LOG.trace("Request parameter: nameCar --> " + nameCar);

		String markCar = request.getParameter("markCar");
		car.setMark(markCar);
		LOG.trace("Request parameter: markCar --> " + markCar);

		String carClass = request.getParameter("carClass");
		car.setCarClass(carClass);
		LOG.trace("Request parameter: carClass --> " + carClass);

		String carPrice = request.getParameter("carPrice");
		int resultPrice = Integer.parseInt(carPrice);
		car.setPrice(resultPrice);
		LOG.trace("Request parameter: carPrice --> " + carPrice);

		new CarDao().addNewCar(car);

		LOG.debug("Commands finished");
		return Path.COMMAND_LIST_CAR_ADMIN;
	}
}