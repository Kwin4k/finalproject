package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Change status to return car command.
 * 
 * @author V.Korsun
 * 
 */
public class ChangeStatusToReturnCarCommand extends Command {

	private static final long serialVersionUID = 5166286214029438535L;

	private static final Logger LOG = Logger.getLogger(ChangeStatusToReturnCarCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();

		Order order = new Order();
		// Status orderStatus = Status.getStatus(order);
		
		if (request.getParameter("repairByOrderId") != null) {
			long orderId = Long.parseLong(request.getParameter("repairByOrderId"));
			LOG.trace("Request parameter: orderId --> " + orderId);
			session.setAttribute("repairOrderId", orderId);
			
			LOG.debug("Command finished");
			return Path.COMMAND_SHOW_ORDER_FOR_REPAIR;
		}
		
		if (request.getParameter("closeByOrderId") != null) {
			long orderId = Long.parseLong(request.getParameter("closeByOrderId"));
			LOG.trace("Request parameter: orderId --> " + orderId);
			order.setStatusId(4);
			LOG.trace("Request parameter: status --> close");

			new OrderDao().updateOrderStatusByOrderId(order, orderId);
			
			LOG.debug("Command finished");
			return Path.COMMAND_LIST_ORDERS;
		}

		LOG.debug("Command finished");
		return Path.PAGE_CHANGE_STATUS_CAR;
	}

}