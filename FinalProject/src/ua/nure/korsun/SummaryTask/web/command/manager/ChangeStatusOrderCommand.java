package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Change status order command.
 * 
 * @author V.Korsun
 * 
 */
public class ChangeStatusOrderCommand extends Command {

	private static final long serialVersionUID = 5136286214029478505L;

	private static final Logger LOG = Logger.getLogger(ChangeStatusOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		
		Order order = new Order();

		if (request.getParameter("inputComment") != null) {
			String comment = request.getParameter("inputComment");
			order.setComment(comment);
			LOG.trace("Request parameter: Comment --> " + comment);
		}

		if (request.getParameter("use") != null && request.getParameter("use").equals("use")) {
			String status = request.getParameter("use");
			order.setStatusId(3);
			LOG.trace("Request parameter: status --> " + status);
		}

		if (request.getParameter("blocked") != null && request.getParameter("blocked").equals("blocked")) {
			String status = request.getParameter("blocked");
			order.setStatusId(1);
			LOG.trace("Request parameter: status --> " + status);
		}
		
		LOG.trace("Request parameter: orderId--> " + session.getAttribute("orderId"));
		
		Long orderId = (Long) session.getAttribute("orderId");

		new OrderDao().updateOrderStatusByOrderId(order, orderId);

		LOG.debug("Command finished");
		return Path.COMMAND_LIST_ORDERS;
	}

}