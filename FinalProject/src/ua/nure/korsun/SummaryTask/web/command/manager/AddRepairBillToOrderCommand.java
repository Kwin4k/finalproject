package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.dao.RepairBillDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.db.entity.RepairBill;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Add repair bill to client order.
 * 
 * @author V.Korsun
 * 
 */
public class AddRepairBillToOrderCommand extends Command {

	private static final long serialVersionUID = 1873972153186581503L;

	private static final Logger LOG = Logger.getLogger(AddRepairBillToOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");

		RepairBill repairBill = new RepairBill();
		Order order = new Order();
		
		long orderId = Long.parseLong(request.getParameter("orderId"));
		repairBill.setOrderId(orderId);
		LOG.trace("Request parameter: orderId  --> " + orderId );

		String repairName = request.getParameter("repairName");
		repairBill.setRepairName(repairName);
		LOG.trace("Request parameter: repairName --> " + repairName);

		int repairPrice = Integer.parseInt(request.getParameter("repairPrice"));
		repairBill.setBill(repairPrice);
		LOG.trace("Request parameter: repairPrice --> " + repairPrice);

		String comment = request.getParameter("comment");
		repairBill.setComment(comment);
		LOG.trace("Request parameter: comment --> " + comment);
		
		order.setStatusId(5);
		LOG.trace("Request parameter: status --> repair");
		
		new OrderDao().updateOrderStatusByOrderId(order, orderId);
		LOG.trace("Update status order successful");
		
		new RepairBillDao().addRepairBillToOrder(repairBill);
		LOG.trace("Add repair order successful");
		
		order = new OrderDao().findOrderById(orderId);
		LOG.trace("Order -->" + order);
		
		Car car = new CarDao().findCarById(order.getCarId());
		LOG.trace("Car -->" + car);
		
		int finalSumForRepair = car.getSumForRepair() + repairPrice;
		LOG.trace("Final sum -->" + finalSumForRepair);
		
		new CarDao().updateCarSumForRepairById(order.getCarId(), finalSumForRepair);
		
		LOG.debug("Commands finished");
		return Path.COMMAND_LIST_ORDERS;
	}
}