package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Delete order command.
 * 
 * @author V.Korsun
 * 
 */
public class DeleteOrderCommand extends Command {

	private static final long serialVersionUID = -5918601941199297325L;

	private static final Logger LOG = Logger.getLogger(DeleteOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();

		if (session.getAttribute("redirectIndex") == null) {

			long orderId = Long.parseLong(request.getParameter("deleteOrder"));

			LOG.trace("Request parameter: order with id --> " + orderId);

			new OrderDao().deleteOrder(orderId);
			LOG.trace("Delete order");

		}
		LOG.debug("Commands finished");
		return Path.COMMAND_ORDER_BASKET;
	}
}