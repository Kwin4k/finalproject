package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBasketBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBasketBeanDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Show client basket order command.
 * 
 * @author V.Korsun
 * 
 */
public class ClientBasketCommand extends Command {

	private static final long serialVersionUID = 3863978454619586513L;

	private static final Logger LOG = Logger.getLogger(ClientBasketCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();

		long clientId = (long) session.getAttribute("clientId");
		LOG.trace("Request parameter: user with id --> " + clientId);
 
		List<ClientOrderBasketBean> basketBean = new ClientOrderBasketBeanDao().getClientOrderBasketBeans(clientId);
		LOG.trace("Found in DB: basketBean --> " + basketBean);

		List<ClientOrderBasketBean> basketBeanRepair = new ClientOrderBasketBeanDao()
				.getClientOrderBasketBeansRepair(clientId);
		LOG.trace("Found in DB: basketBeanRepair --> " + basketBeanRepair);

		session.setAttribute("basketBean", basketBean);
		session.setAttribute("basketBeanRepair", basketBeanRepair);
		session.setAttribute("redirectIndex", 1);

		LOG.debug("Commands finished");
		return Path.PAGE_CLIENT_BASKET;
	}
}