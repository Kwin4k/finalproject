package ua.nure.korsun.SummaryTask.web.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBeanDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Select orders command.
 * 
 * @author V.Korsun
 * 
 */
public class SelectOrdersCommand extends Command {

	private static final long serialVersionUID = 1732786214529178505L;

	private static final Logger LOG = Logger.getLogger(SelectOrdersCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
 
		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		List<ClientOrderBean> clientOrderBeanList = new ArrayList<>();

		if (session.getAttribute("redirectIndex") == null) {

			if (!request.getParameter("byClientId").equals("null")) {
				String selectByClientId = request.getParameter("byClientId");

				LOG.trace("SelectByClientId --> " + selectByClientId);

				clientOrderBeanList = new ClientOrderBeanDao().findOrdersSelectByClientId(selectByClientId);
				LOG.trace("Found in DB: clientOrderBeanList --> " + clientOrderBeanList);
			}
			
			if (!request.getParameter("byStatus").equals("null")) {
				String selectByStatus = request.getParameter("byStatus");

				LOG.trace("SelectByStatus --> " + selectByStatus );

				clientOrderBeanList = new ClientOrderBeanDao().findOrdersSelectByStatus(selectByStatus);
				LOG.trace("Found in DB: clientOrderBeanList --> " + clientOrderBeanList);
			}

			session.setAttribute("redirect", clientOrderBeanList);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "clientOrderBeanList");
		}
		LOG.debug("Command finished");
		return Path.PAGE_LIST_ORDERS;
	}

}