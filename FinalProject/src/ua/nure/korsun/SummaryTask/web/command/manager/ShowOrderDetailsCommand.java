package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientDao;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBeanDao;
import ua.nure.korsun.SummaryTask.db.entity.Client;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Show order detail command.
 * 
 * @author V.Korsun
 * 
 */
public class ShowOrderDetailsCommand extends Command {

	private static final long serialVersionUID = 5136286214029478505L;

	private static final Logger LOG = Logger.getLogger(ShowOrderDetailsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {

			String orderId = request.getParameter("orderId");
			long resultId = Long.parseLong(orderId);
			LOG.trace("Request parameter: orderId --> " + resultId);

			session.removeAttribute(orderId);
			session.setAttribute("orderId", resultId);

			ClientOrderBean clientOrderBeanByOrderId = new ClientOrderBeanDao().getClientOrderBeanByOrderId(resultId);
			LOG.trace("Found in DB: orderBean--> " + clientOrderBeanByOrderId);

			session.setAttribute("orderBean", clientOrderBeanByOrderId);

			request.setAttribute("orderBean", clientOrderBeanByOrderId);
			LOG.trace("Set the request attribute: orderBean --> " + clientOrderBeanByOrderId);

			int clientId = clientOrderBeanByOrderId.getClientId();

			Client client = new ClientDao().findClientById(clientId);
			LOG.trace("Found in DB: client--> " + client);

			session.setAttribute("client", client);

			session.setAttribute("redirectIndex", 1);
		}

		LOG.debug("Command finished");
		return Path.PAGE_SHOW_DETAIL_ORDER;
	}

}