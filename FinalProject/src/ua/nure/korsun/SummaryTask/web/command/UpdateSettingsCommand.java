package ua.nure.korsun.SummaryTask.web.command;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.exception.DBException;
import ua.nure.korsun.SummaryTask.exception.Messages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

/**
 * Update settings items.
 * 
 * @author V.Korsun
 * 
 */
public class UpdateSettingsCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger LOG = Logger.getLogger(UpdateSettingsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");

		User user = (User) request.getSession().getAttribute("user");
		boolean updateUser = false;

		// update first name
		String firstName = request.getParameter("firstName");
		if (firstName != null && !firstName.isEmpty()) {
			user.setFirstName(firstName);
			updateUser = true;
			LOG.trace("First name --> " + firstName);
		}

		// update last name
		String lastName = request.getParameter("lastName");
		if (lastName != null && !lastName.isEmpty()) {
			user.setLastName(lastName);
			updateUser = true;
			LOG.trace("Last name --> " + lastName);
		}

		String localeToSet = request.getParameter("localeToSet");
		if (localeToSet != null && !localeToSet.isEmpty()) {
			HttpSession session = request.getSession();
			Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", localeToSet);
			session.setAttribute("defaultLocale", localeToSet);
			user.setLocaleName(localeToSet);
			updateUser = true;
		}

		User newUser = new UserDao().findUserById(user.getId());
		LOG.trace("Get User by Id --> " + newUser);

		String password = request.getParameter("password");
		LOG.trace("Password --> " + password);

		if (password != null && !password.isEmpty()) {
			if (newUser.getPassword().equals(password)) {
				String newPassword = request.getParameter("newPassword");
				if (newPassword != null && !newPassword.isEmpty()) {
					user.setPassword(newPassword);
					updateUser = true;
					LOG.trace("New Password --> " + newPassword);
				}
			} else {
				LOG.error(Messages.ERR_PASSWORD_MISMATCH);
				throw new AppException(Messages.ERR_PASSWORD_MISMATCH);
			}
		}

		if (updateUser == true) {
			new UserDao().updateUser(user);
			LOG.trace("Update into DB: user --> " + user);
		}

		LOG.debug("Command finished");
		return Path.PAGE_SETTINGS;
	}
}