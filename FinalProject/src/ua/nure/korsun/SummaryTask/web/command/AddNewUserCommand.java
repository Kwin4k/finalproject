package ua.nure.korsun.SummaryTask.web.command;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.Role;
import ua.nure.korsun.SummaryTask.db.dao.ClientDao;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.AppException;

/**
 * Add new user command.
 * 
 * @author V.Korsun
 * 
 */
public class AddNewUserCommand extends Command {

	private static final long serialVersionUID = 3263078457619586513L;

	private static final Logger LOG = Logger.getLogger(AddNewUserCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");

		HttpSession session = request.getSession();		
		User user = new User();
		
		String firstName = request.getParameter("firstName");
		user.setFirstName(firstName);
		LOG.trace("Request parameter: firstName --> " + firstName);
		
		String lastName = request.getParameter("lastName");
		user.setLastName(lastName);
		LOG.trace("Request parameter: lastName --> " + lastName);

		String login = request.getParameter("login");
		user.setLogin(login);
		LOG.trace("Request parameter: login --> " + login);

		String password = request.getParameter("password");
		LOG.trace("Request parameter: password --> ******");
		
		String password2 = request.getParameter("password2");
		LOG.trace("Request parameter: password2 --> ******2");
		
		if(!password.equals(password2)) {
			LOG.error("password !=password2");
			return Path.PAGE_ERROR_PAGE;
		}else {
			user.setPassword(password);
		}
		
		user.setRoleId(2);
		LOG.trace("Set user role: user role --> client");

		new UserDao().addUser(user); 
		
		LOG.trace("Add to DB: user --> " + user);
		
		user = new UserDao().findUserByLogin(login);
		
		LOG.trace("Get from DB: user --> " + user);

		Role userRole = Role.getRole(user);
		LOG.trace("userRole --> " + userRole);

		if (userRole == Role.CLIENT) {		
			session.setAttribute("clientId", user.getId());
			LOG.trace("Request parameter: client id --> " + user.getId());
			new ClientDao().addUserId2Client(user.getId());
		}

		session.setAttribute("user", user);
		LOG.trace("Set the session attribute: user --> " + user);

		session.setAttribute("userRole", userRole);
		LOG.trace("Set the session attribute: userRole --> " + userRole);

		LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());

		LOG.debug("Command finished");
		return Path.PAGE_LOGIN;
	}
}