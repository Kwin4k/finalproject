package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBasketBean;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.dao.ClientDao;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.db.entity.Client;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Pay order command.
 * 
 * @author V.Korsun
 * 
 */
public class ClientPayOrderCommand extends Command {

	private static final long serialVersionUID = 7030280214129478505L;

	private static final Logger LOG = Logger.getLogger(ClientPayOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		ClientOrderBasketBean orderBean = new ClientOrderBasketBean();
		Client client = new Client();

		String passportId = request.getParameter("passportId");
		client.setPassportId(passportId);
		LOG.trace("Request parameter: passport id --> " + passportId);

		String age = request.getParameter("age");
		int resultAge = Integer.parseInt(age);
		client.setAge(resultAge);
		LOG.trace("Request parameter: age --> " + age);

		String address = request.getParameter("address");
		client.setAddress(address);
		LOG.trace("Request parameter: adress --> " + address);

		String phone = request.getParameter("phone");
		client.setPhone(phone);
		LOG.trace("Request parameter: phone --> " + phone);

		String email = request.getParameter("email");
		client.setEmail(email);
		LOG.trace("Request parameter: email --> " + email);

		HttpSession session = request.getSession();

		long clientId = (long) session.getAttribute("clientId");
		client.setId(clientId);
		LOG.trace("Request parameter: client with id --> " + clientId);

		new ClientDao().updateClient(client); 

		request.setAttribute("clientPayOrderCommand", client);
		LOG.trace("Set the request attribute: clientPayOrderCommand --> " + client);

		String orderId = request.getParameter("payOrder");
		int resultId = Integer.parseInt(orderId);
		LOG.trace("Request parameter: order id --> " + resultId);

		new OrderDao().updateClientOrder(resultId);
		
		Order order = new OrderDao().findOrderById(resultId);
		
		LOG.trace("car id --> " + order.getCarId());
		
		Car car = new CarDao().findCarById(order.getCarId());
		
		LOG.trace("Car --> " + car);
		
		int numberOfUses = car.getNumberOfUses();
		LOG.trace("Num of uses --> " + numberOfUses);
		
		numberOfUses++;
		LOG.trace("Num of uses --> " + numberOfUses);
		
		new CarDao().updateCarNumOfUsesById(order.getCarId(), numberOfUses);

		request.setAttribute("clientPayOrderCommand", orderBean);
		LOG.trace("Set the request attribute: clientPayOrderCommand --> " + orderBean);

		LOG.debug("Command finished");
		return Path.COMMAND_LIST_CAR;
	}

}