package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * List cars items.
 * 
 * @author V.Korsun
 * 
 */
public class ListCarsCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger LOG = Logger.getLogger(ListCarsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {

			List<Car> car = new CarDao().findCarItems();
			LOG.trace("Found in DB: carList --> " + car);

			if (request.getParameter("sortBy") != null) {
				String sortBy = request.getParameter("sortBy");
				String direction = request.getParameter("direction");

				if (sortBy.equals("id")) {
					Collections.sort(car, new Comparator<Car>() {
						public int compare(Car o1, Car o2) {
							if (direction.equals("up")) {
								return (int) (o2.getId() - o1.getId());
							} else {
								return (int) (o1.getId() - o2.getId());
							}
						}
					});
				}

				if (sortBy.equals("price")) {
					Collections.sort(car, new Comparator<Car>() {
						public int compare(Car o1, Car o2) {
							if (direction.equals("up")) {
								return (int) (o2.getPrice() - o1.getPrice());
							} else {
								return (int) (o1.getPrice() - o2.getPrice());
							}
						}
					});
				}

				if (sortBy.equals("name")) {
					Collections.sort(car, new Comparator<Car>() {
						public int compare(Car o1, Car o2) {
							if (direction.equals("big")) {
								return (int) (o2.getName().compareTo(o1.getName()));
							} else {
								return (int) (o1.getName().compareTo(o2.getName()));
							}
						}
					});
				}

				if (sortBy.equals("class")) {
					Collections.sort(car, new Comparator<Car>() {
						public int compare(Car o1, Car o2) {
							if (direction.equals("up")) {
								return (int) (o2.getCarClass().compareTo(o1.getCarClass()));
							} else {
								return (int) (o1.getCarClass().compareTo(o2.getCarClass()));
							}
						}
					});
				}

			} else {
				Collections.sort(car, new Comparator<Car>() {
					public int compare(Car o1, Car o2) {
						return (int) (o1.getId() - o2.getId());
					}
				});
			}

			session.setAttribute("redirect", car);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "car");

			List<Car> carClass = new CarDao().findCarsClass();
			LOG.trace("Found in DB: carListClass --> " + carClass);

			session.setAttribute("carClass", carClass);
			
			List<Car> carMark = new CarDao().findCarsMark();
			LOG.trace("Found in DB: carListMark --> " + carMark);

			session.setAttribute("carMark", carMark);

		}
		LOG.debug("Command finished");
		return Path.PAGE_LIST_CAR;
	}
}