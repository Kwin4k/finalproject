package ua.nure.korsun.SummaryTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.Role;
import ua.nure.korsun.SummaryTask.db.dao.ClientDao;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.db.entity.Client;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.AppException;

/**
 * Login command.
 * 
 * @author V.Korsun
 * 
 */
public class LoginCommand extends Command {

	private static final long serialVersionUID = -3071536593627692473L;

	private static final Logger LOG = Logger.getLogger(LoginCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException, AppException {
		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		// obtain login and password from a request 
		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);

		String password = request.getParameter("password");
		if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
			throw new AppException("Login/password cannot be empty");
		}

		User user = new UserDao().findUserByLogin(login);
		LOG.trace("Found in DB: user --> " + user);

		if (user == null || !password.equals(user.getPassword())) {
			throw new AppException("Cannot find user with such login/password");
		}

		Role userRole = Role.getRole(user);
		LOG.trace("userRole --> " + userRole);
		
		String forward = Path.PAGE_ERROR_PAGE;

		if (userRole == Role.ADMIN) {
			forward = Path.COMMAND_LIST_CAR_ADMIN;
			session.setAttribute("admin", "admin");
			LOG.trace("Set in ssesion user role --> " +  "admin");
		}

		if (userRole == Role.CLIENT) {
			forward = Path.COMMAND_LIST_CAR;	
			Client client = new ClientDao().findClientByUserId(user.getId());
			session.setAttribute("clientId", client.getId());
			LOG.trace("Request parameter: client id --> " + client.getId());
			LOG.trace("Request parameter: user id --> " + user.getId());
		}
		
		if (userRole == Role.MANAGER) {
			forward = Path.COMMAND_LIST_ORDERS;
			session.setAttribute("manager", Role.MANAGER);
			LOG.trace("Set in ssesion user role --> " + Role.MANAGER);
		}	

		session.setAttribute("user", user);
		LOG.trace("Set the session attribute: user --> " + user);

		session.setAttribute("userRole", userRole);
		LOG.trace("Set the session attribute: userRole --> " + userRole);

		LOG.info(user + " logged as " + userRole.toString().toLowerCase());	
		
		String userLocaleName = user.getLocaleName();
		LOG.trace("userLocalName --> " + userLocaleName);
		
		if (userLocaleName != null && !userLocaleName.isEmpty()) {
			Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
			
			session.setAttribute("defaultLocale", userLocaleName);
			LOG.trace("Set the session attribute: defaultLocaleName --> " + userLocaleName);
			
			LOG.info("Locale for user: defaultLocale --> " + userLocaleName);
		}

		LOG.debug("Command finished");
		return forward;
	}

}