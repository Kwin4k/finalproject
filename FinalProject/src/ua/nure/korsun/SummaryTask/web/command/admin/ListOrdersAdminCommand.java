package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBeanDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * List orders items.
 * 
 * @author V.Korsun
 * 
 */
public class ListOrdersAdminCommand extends Command {

	private static final long serialVersionUID = 1863928254189556513L;

	private static final Logger LOG = Logger.getLogger(ListOrdersAdminCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {

			List<ClientOrderBean> clientOrderBeanList = new ClientOrderBeanDao().getClientOrderBeans();
			LOG.trace("Found in DB: clientOrderBeanList --> " + clientOrderBeanList);

			session.setAttribute("redirect", clientOrderBeanList);
			LOG.trace("Set Attribute value --> " + clientOrderBeanList);
			
			session.setAttribute("redirectIndex", 1);
			LOG.trace("Set Attribute index" + 1);
			
			session.setAttribute("redirectName", "clientOrderBeanList");
			LOG.trace("Set Attribute name --> " + "clientOrderBeanList");
		}

		LOG.debug("Commands finished");
		return Path.PAGE_LIST_ORDERS_ADMIN;
	}

}