package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Delete manager command.
 * 
 * @author V.Korsun
 * 
 */
public class DeleteManagerCommand extends Command {

	private static final long serialVersionUID = 827955570191527868L;
	
	private static final Logger LOG = Logger.getLogger(DeleteManagerCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();

		if (session.getAttribute("redirectIndex") == null) {

			long userId = Long.parseLong(request.getParameter("deleteManager"));

			LOG.trace("Request parameter: manager with id --> " + userId);
 
			new UserDao().deleteUser(userId);
			LOG.trace("Delete manager");

		}
		LOG.debug("Commands finished");
		return Path.COMMAND_LIST_MANAGERS;
	}
}