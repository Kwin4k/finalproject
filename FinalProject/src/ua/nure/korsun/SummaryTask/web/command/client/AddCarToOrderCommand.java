package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.dao.ServiceDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.db.entity.Service;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Add car to order command.
 * 
 * @author V.Korsun
 * 
 */
public class AddCarToOrderCommand extends Command {

	private static final long serialVersionUID = 1863978254689586513L;

	private static final Logger LOG = Logger.getLogger(AddCarToOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");
		HttpSession session = request.getSession();

		if (session.getAttribute("redirectIndex") == null) {

			String itemCarId = request.getParameter("itemId");
			int resultId = Integer.parseInt(itemCarId);
			LOG.trace("Request parameter: carId --> " + itemCarId);

			session.setAttribute("carId", resultId);
			LOG.debug("Init in session client car");

			LOG.debug("Commands starts");
			Car car = new CarDao().findCarById(resultId);
			LOG.trace("Found in DB: client car --> " + car);
			
			List<Service> serviceList = new ServiceDao().getListServices();
			
			session.setAttribute("serviceList", serviceList);
			LOG.debug("Init in session serviceList");

			session.setAttribute("redirect", car);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "car");
		}
		LOG.debug("Commands finished");
		return Path.PAGE_ADD_CLIENT_ORDER;
	}
}