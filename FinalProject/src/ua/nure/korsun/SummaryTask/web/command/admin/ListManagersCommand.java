package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * List manager items.
 * 
 * @author V.Korsun
 * 
 */
public class ListManagersCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger LOG = Logger.getLogger(ListManagersCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {

			List<User> managers = new UserDao().findManagers();
			LOG.trace("Found in DB: managersList --> " + managers);

			Collections.sort(managers, new Comparator<User>() {
				public int compare(User o1, User o2) {
					return (int) (o1.getId() - o2.getId());
				}
			});

			session.setAttribute("redirect", managers);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "managers");
		}
		
		LOG.debug("Command finished");
		return Path.PAGE_LIST_MANAGERS;
	}

}