package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * List car for admin items.
 * 
 * @author V.Korsun
 * 
 */
public class ListCarsAdminCommand extends Command {

	private static final long serialVersionUID = 6732786214029178505L;

	private static final Logger LOG = Logger.getLogger(ListCarsAdminCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession(); 

		if (session.getAttribute("redirectIndex") == null) {

			List<Car> car = new CarDao().findCarItems();
			LOG.trace("Found in DB: carList --> " + car);

			Collections.sort(car, new Comparator<Car>() {
				public int compare(Car o1, Car o2) {
					return (int) (o1.getId() - o2.getId());
				}
			});

			if (session.getAttribute("index") == null) {
				request.setAttribute("index", 0);
				LOG.trace("Set the session attribute: index--> " + 0);
			} else {
				long index = (Long) session.getAttribute("index");
				request.setAttribute("index", index);
				LOG.trace("Set the request attribute: index--> " + index);
			}
			
			List<Car> carClass = new CarDao().findCarsClass();
			LOG.trace("Found in DB: carListClass --> " + carClass);

			session.setAttribute("carClass", carClass);
			
			List<Car> carMark = new CarDao().findCarsMark();
			LOG.trace("Found in DB: carListMark --> " + carMark);

			session.setAttribute("carMark", carMark);

			session.setAttribute("redirect", car);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "car");
		}
		LOG.debug("Command finished");
		return Path.PAGE_LIST_CAR_ADMIN;
	}

}