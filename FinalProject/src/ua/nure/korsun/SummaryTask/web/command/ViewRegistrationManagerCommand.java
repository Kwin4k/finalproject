package ua.nure.korsun.SummaryTask.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;

/**
 * View registration manager command.
 * 
 * @author V.Korsun
 * 
 */
public class ViewRegistrationManagerCommand extends Command {
	
	private static final long serialVersionUID = -3071533593647592473L;
	
	private static final Logger LOG = Logger.getLogger(ViewRegistrationManagerCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {		
		LOG.debug("Command starts");

		LOG.debug("Command finished");
		return Path.PAGE_ADD_MANAGER;
	}
}