package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.Status;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBeanDao;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Lists orders.
 * 
 * @author V.Korsun
 * 
 */
public class ListOrdersCommand extends Command {

	private static final long serialVersionUID = 1863978254689586513L;

	private static final Logger LOG = Logger.getLogger(ListOrdersCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {

			List<ClientOrderBean> clientOrderBeanList = new ClientOrderBeanDao().getClientOrderBeans();
			LOG.trace("Found in DB: clientOrderBeanList --> " + clientOrderBeanList);

			List<Order> listClientId = new OrderDao().getListClientId();
			LOG.trace("Found in DB: listClientId --> " + listClientId);

			List<Order> listStatus = new OrderDao().getListStatusOrder();
			LOG.trace("Found in DB: listStatus --> " + listStatus);

			session.setAttribute("clientOrderBeanList", clientOrderBeanList);
			LOG.trace("Set Attribute value --> " + clientOrderBeanList);

			Status[] orderStatus = Status.values();

			session.setAttribute("orderStatus", orderStatus);

			session.setAttribute("redirectIndex", 1);
			LOG.trace("Set Attribute index --> " + 1);

			session.setAttribute("listClientId", listClientId);
			LOG.trace("Set Attribute listClientId --> " + listClientId);

			session.setAttribute("listStatus", listStatus);
			LOG.trace("Set Attribute listStatus --> " + listStatus);

		}

		LOG.debug("Commands finished");
		return Path.PAGE_LIST_ORDERS;
	}

}