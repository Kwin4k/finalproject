package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Change car command.
 * 
 * @author V.Korsun
 * 
 */
public class ChangeCarCommand extends Command {

	private static final long serialVersionUID = 1853972254189581513L;

	private static final Logger LOG = Logger.getLogger(ChangeCarCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");

		HttpSession session = request.getSession();

		if (request.getParameter("itemId") != null) {
			String itemId = request.getParameter("itemId");
			long index = Long.parseLong(itemId);
			session.setAttribute("index", index);
			LOG.trace("Session parameter: index --> " + index);
		} else {
			if (request.getParameter("carId") == null) {
				String carId = request.getParameter("deleteCarId");
				long resultId = Long.parseLong(carId);
				LOG.trace("Request parameter: deleteCarId --> " + resultId);

				new CarDao().deleteCarById(resultId); 
				
				session.setAttribute("index", null);
				LOG.trace("Session parameter: index --> " + null);
			} else {
				Car car = new Car();

				String carId = request.getParameter("carId");
				long resultId = Long.parseLong(carId);
				car.setId(resultId);
				LOG.trace("Request parameter: carId --> " + resultId);

				String nameCar = request.getParameter("nameCar");
				car.setName(nameCar);
				LOG.trace("Request parameter: nameCar --> " + nameCar);

				String markCar = request.getParameter("markCar");
				car.setMark(markCar);
				LOG.trace("Request parameter: markCar --> " + markCar);

				String carClass = request.getParameter("carClass");
				car.setCarClass(carClass);
				LOG.trace("Request parameter: carClass --> " + carClass);

				String carPrice = request.getParameter("carPrice");
				int resultPrice = Integer.parseInt(carPrice);
				car.setPrice(resultPrice);
				LOG.trace("Request parameter: carPrice --> " + carPrice);

				new CarDao().changeCarById(car);

				session.setAttribute("index", null);
				LOG.trace("Session parameter: index --> " + null);
			}
		}

		LOG.debug("Commands finished");
		return Path.COMMAND_LIST_CAR_ADMIN;
	}
}