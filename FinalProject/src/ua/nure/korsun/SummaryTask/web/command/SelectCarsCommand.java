package ua.nure.korsun.SummaryTask.web.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Select cars command.
 * 
 * @author V.Korsun
 * 
 */
public class SelectCarsCommand extends Command {

	private static final long serialVersionUID = 2732786214029178505L;

	private static final Logger LOG = Logger.getLogger(SelectCarsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();
		List<Car> car = new ArrayList<>();

		if (session.getAttribute("redirectIndex") == null) {

			if (request.getParameter("byMark").equals("0")) {
				String selectByClass = request.getParameter("byClass");

				LOG.trace("Found in DB: selectByClass --> " + selectByClass);

				car = new CarDao().findCarsBySelectClass(selectByClass);
				LOG.trace("Found in DB: selectCar --> " + car);
			}
			
			if (request.getParameter("byClass").equals("0")) {
				String selectByMark = request.getParameter("byMark");

				LOG.trace("Found in DB: selectByMark --> " + selectByMark );

				car = new CarDao().findCarsBySelectMark(selectByMark );
				LOG.trace("Found in DB: selectCar --> " + car);
			}

			session.setAttribute("redirect", car);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "car");
		}
		
		String userRole = (String) session.getAttribute("admin");
		LOG.trace("userRole --> " + userRole);

		if (userRole != null && userRole.equals("admin")) {
			LOG.debug("Command finished");
			return Path.PAGE_LIST_CAR_ADMIN;
		}
		
		LOG.debug("Command finished");
		return Path.PAGE_LIST_CAR;
	}

}