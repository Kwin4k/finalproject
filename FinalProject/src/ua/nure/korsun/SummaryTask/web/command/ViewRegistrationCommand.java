package ua.nure.korsun.SummaryTask.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;

/**
 * View registration command.
 * 
 * @author V.Korsun
 * 
 */
public class ViewRegistrationCommand extends Command {
	
	private static final long serialVersionUID = -3071536593627692473L;
	
	private static final Logger LOG = Logger.getLogger(ViewRegistrationCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {		
		LOG.debug("Command starts");

		LOG.debug("Command finished");
		return Path.PAGE_USER_REGISTRATION;
	}

}