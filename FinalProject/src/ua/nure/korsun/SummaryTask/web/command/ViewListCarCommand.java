package ua.nure.korsun.SummaryTask.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;

/**
 * View list car command.
 * 
 * @author V.Korsun
 * 
 */
public class ViewListCarCommand extends Command {
	
	private static final long serialVersionUID = -8181487310853932302L;
	
	private static final Logger LOG = Logger.getLogger(ViewListCarCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {		
		LOG.debug("Command starts"); 

		LOG.debug("Command finished"); 
		return Path.COMMAND_LIST_CAR;
	}

}