package ua.nure.korsun.SummaryTask.web.command.client;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.CarDao;
import ua.nure.korsun.SummaryTask.db.dao.OrderDao;
import ua.nure.korsun.SummaryTask.db.dao.ServiceDao;
import ua.nure.korsun.SummaryTask.db.entity.Car;
import ua.nure.korsun.SummaryTask.db.entity.Order;
import ua.nure.korsun.SummaryTask.db.entity.Service;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Add car features command.
 * 
 * @author V.Korsun
 * 
 */
public class AddCarFeaturesCommand extends Command {

	private static final long serialVersionUID = 7030280214129478505L;

	private static final Logger LOG = Logger.getLogger(AddCarFeaturesCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		HttpSession session = request.getSession();

		if (session.getAttribute("redirectIndex") == null) {
			Order order = new Order();

			String days = request.getParameter("days");
			int resultDays = Integer.parseInt(days);
			order.setCountDays(resultDays);
			LOG.trace("Request parameter: days --> " + days);

			String color = request.getParameter("color");
			order.setColor(color);
			LOG.trace("Request parameter: color --> " + color);

			List<Service> serviceList = new ServiceDao().getListServices();
			int sumPrice = 0;

			for (int i = 0; i < serviceList.size(); i++) {
				if (request.getParameter(String.valueOf(serviceList.get(i).getId())) != null) {
					int servicePrice = Integer
							.parseInt(request.getParameter(String.valueOf(serviceList.get(i).getId())));
					sumPrice += servicePrice;
				}
			}
			LOG.trace("Request parameter: sumPriceService --> " + sumPrice);

			long clientId = (long) session.getAttribute("clientId");
			int resultClientId = (int) clientId;
			order.setClientId(resultClientId);
			LOG.trace("Session parameter: clientId --> " + clientId);

			int carId = (int) session.getAttribute("carId");
			order.setCarId(carId);
			LOG.trace("Session parameter: carId --> " + carId);

			order.setStatusId(0);
			LOG.trace("Set order status --> opened");

			Car car = new CarDao().findCarById(carId);

			int carForDays = car.getPrice() * resultDays;
			int finalPrice = sumPrice + carForDays;
			order.setBill(finalPrice);
			LOG.trace("Final bill for car --> " + finalPrice);

			new OrderDao().addClientOrder(order);
			LOG.trace("Set the request attribute: formClientOrderCommand --> " + order);

			// long orderId = DBManager.getInstance().getPriceByCarId(carId);

			session.setAttribute("redirect", order);
			session.setAttribute("redirectIndex", 1);
			session.setAttribute("redirectName", "formClientOrderCommand");

		}
		LOG.debug("Command finished");
		return Path.COMMAND_LIST_CAR;
	}

}