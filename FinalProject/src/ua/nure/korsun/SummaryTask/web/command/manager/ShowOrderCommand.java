package ua.nure.korsun.SummaryTask.web.command.manager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.bean.ClientOrderBean;
import ua.nure.korsun.SummaryTask.db.dao.ClientOrderBeanDao;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Show orders.
 * 
 * @author V.Korsun
 * 
 */
public class ShowOrderCommand extends Command {

	private static final long serialVersionUID = 1863978254689586513L;

	private static final Logger LOG = Logger.getLogger(ShowOrderCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Commands starts");
		HttpSession session = request.getSession();
		LOG.debug("Init session");

		if (session.getAttribute("redirectIndex") == null) {
			long orderid = (Long)session.getAttribute("repairOrderId");

			ClientOrderBean clientOrderBean = new ClientOrderBeanDao().getClientOrderBeanByOrderId(orderid);
			LOG.trace("Found in DB: clientOrderBean --> " + clientOrderBean);

			session.setAttribute("redirect", clientOrderBean);
			LOG.trace("Set Attribute value --> " + clientOrderBean);
			
			session.setAttribute("redirectIndex", 1);
			LOG.trace("Set Attribute index" + 1);
			
			session.setAttribute("redirectName", "clientOrderBean");
			LOG.trace("Set Attribute name --> " + "clientOrderBean");
		}

		LOG.debug("Commands finished");
		return Path.PAGE_CHANGE_STATUS_CAR;
	}

}