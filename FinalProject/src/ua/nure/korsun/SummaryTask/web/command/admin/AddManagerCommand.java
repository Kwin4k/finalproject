package ua.nure.korsun.SummaryTask.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.Path;
import ua.nure.korsun.SummaryTask.db.dao.UserDao;
import ua.nure.korsun.SummaryTask.db.entity.User;
import ua.nure.korsun.SummaryTask.exception.AppException;
import ua.nure.korsun.SummaryTask.web.command.Command;

/**
 * Add new manager command.
 * 
 * @author V.Korsun
 * 
 */
public class AddManagerCommand extends Command {

	private static final long serialVersionUID = 3263078457619586513L;

	private static final Logger LOG = Logger.getLogger(AddManagerCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");

		User user = new User();

		String firstName = request.getParameter("firstName");
		user.setFirstName(firstName);
		LOG.trace("Request parameter: firstName --> " + firstName);

		String lastName = request.getParameter("lastName");
		user.setLastName(lastName);
		LOG.trace("Request parameter: lastName --> " + lastName);

		String login = request.getParameter("login");
		user.setLogin(login);
		LOG.trace("Request parameter: login --> " + login);

		String password = request.getParameter("password");
		LOG.trace("Request parameter: password --> ******");

		String password2 = request.getParameter("password2");
		LOG.trace("Request parameter: password2 --> ******2");

		if (!password.equals(password2)) {
			LOG.error("password !=password2");
			return Path.PAGE_ERROR_PAGE;
		} else {
			user.setPassword(password);
			LOG.trace("User password --> " + password);
		}

		user.setRoleId(1);

		new UserDao().addUser(user);

		LOG.trace("Add to DB: manager --> " + user);

		LOG.debug("Command finished");
		return Path.COMMAND_LIST_MANAGERS;
	}
}