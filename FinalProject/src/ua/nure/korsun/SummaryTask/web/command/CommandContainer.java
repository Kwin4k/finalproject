package ua.nure.korsun.SummaryTask.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import ua.nure.korsun.SummaryTask.web.command.admin.AddManagerCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.AddNewCarCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.BlockUserCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.ListCarsAdminCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.ListManagersCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.ListOrdersAdminCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.ListUsersCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.ChangeCarCommand;
import ua.nure.korsun.SummaryTask.web.command.admin.DeleteManagerCommand;

import ua.nure.korsun.SummaryTask.web.command.client.AddCarFeaturesCommand;
import ua.nure.korsun.SummaryTask.web.command.client.AddCarToOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.client.ClientBasketCommand;
import ua.nure.korsun.SummaryTask.web.command.client.ClientPayOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.client.DeleteOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.client.ListCarsCommand;

import ua.nure.korsun.SummaryTask.web.command.manager.ListOrdersCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.ListReturnCarsManagerCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.ShowOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.ChangeStatusToReturnCarCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.AddRepairBillToOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.ChangeStatusOrderCommand;
import ua.nure.korsun.SummaryTask.web.command.manager.ShowOrderDetailsCommand;

/**
 * Holder for all commands.<br/>
 * 
 * @author V.Korsun
 * 
 */
public class CommandContainer {
	
	private static final Logger LOG = Logger.getLogger(CommandContainer.class);
	
	private static Map<String, Command> commands = new TreeMap<String, Command>();
	
	static {
		// common commands
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("addNewUser", new AddNewUserCommand());
		commands.put("selectCar", new SelectCarsCommand());
		commands.put("selectOrder", new SelectOrdersCommand());
		commands.put("viewSettings", new ViewSettingsCommand());
		commands.put("viewRegistration", new ViewRegistrationCommand());
		commands.put("viewListCar", new ViewListCarCommand());
		commands.put("updateSettings", new UpdateSettingsCommand());
		commands.put("noCommand", new NoCommand());
		
		// client commands
		commands.put("listCar", new ListCarsCommand());
		commands.put("addClientId", new AddCarToOrderCommand());
		commands.put("formClientOrderCommand", new AddCarFeaturesCommand());
		commands.put("clientOrderBasketCommand", new ClientBasketCommand());
		commands.put("clientPayOrderCommand", new ClientPayOrderCommand());
		commands.put("deleteOrder", new DeleteOrderCommand());
		
		// admin commands
		commands.put("listCarAdmin", new ListCarsAdminCommand());
		commands.put("listOrdersAdmin", new ListOrdersAdminCommand());
		commands.put("changeCar", new ChangeCarCommand());
		commands.put("blockUser", new BlockUserCommand());
		commands.put("listUsers", new ListUsersCommand());
		commands.put("listManagers", new ListManagersCommand());
		commands.put("addManager", new AddManagerCommand());
		commands.put("viewRegistrationManager", new ViewRegistrationManagerCommand());
		commands.put("addNewCar", new AddNewCarCommand());
		commands.put("deleteManager", new DeleteManagerCommand());
		
		// manager commands
		commands.put("listOrders", new ListOrdersCommand());
		commands.put("listReturnCars", new ListReturnCarsManagerCommand());
		commands.put("addRepair", new AddRepairBillToOrderCommand());
		commands.put("showOrderForRepair", new ShowOrderCommand());
		commands.put("changeStatusToReturnCar", new ChangeStatusToReturnCarCommand());
		commands.put("clientDetail", new ShowOrderDetailsCommand());
		commands.put("changeStatus", new ChangeStatusOrderCommand());
		
		LOG.debug("Command container was successfully initialized");
		LOG.trace("Number of commands --> " + commands.size());
	}


	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			LOG.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand"); 
		}
		
		return commands.get(commandName);
	}
	
}