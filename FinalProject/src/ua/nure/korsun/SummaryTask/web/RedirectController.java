package ua.nure.korsun.SummaryTask.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Main servlet controller.
 * 
 * @author D.Kolesnikov
 * 
 */
public class RedirectController extends HttpServlet {

	private static final long serialVersionUID = 2423353715955164816L;

	private static final Logger LOG = Logger.getLogger(RedirectController.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LOG.debug("Controller starts");

		HttpSession session = request.getSession();
		
		// response.setIntHeader("Refresh", 1);

		String forward = request.getParameter("f");

		if (session.getAttribute("redirectName") != null || session.getAttribute("redirect") != null) {
			request.setAttribute((String) session.getAttribute("redirectName"), session.getAttribute("redirect"));
		}

		LOG.trace("Redirect address --> " + forward);

		LOG.debug("Controller finished, now go to redirect address --> " + forward);

		RequestDispatcher view = request.getRequestDispatcher(forward);

		view.forward(request, response);
	}

}