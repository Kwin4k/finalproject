package ua.nure.korsun.SummaryTask;

/**
 * Path holder (jsp pages, controller commands).
 * 
 * @author V.Korsun
 * 
 */
public final class Path {
 
	// pages
	public static final String PAGE_LOGIN = "/login.jsp";
	public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
	public static final String PAGE_USER_REGISTRATION = "registration.jsp";
	public static final String PAGE_SETTINGS = "/WEB-INF/jsp/settings.jsp";
	
	// client pages
	public static final String PAGE_LIST_CAR = "/WEB-INF/jsp/client/list_car.jsp";
	public static final String PAGE_ADD_CLIENT_ORDER = "/WEB-INF/jsp/client/client_order.jsp";
	public static final String PAGE_CLIENT_BASKET = "/WEB-INF/jsp/client/customer_basket.jsp";
	
	// admin pages
	public static final String PAGE_LIST_CAR_ADMIN = "/WEB-INF/jsp/admin/list_car_admin.jsp";
	public static final String PAGE_LIST_USERS = "/WEB-INF/jsp/admin/list_users.jsp";
	public static final String PAGE_LIST_MANAGERS = "/WEB-INF/jsp/admin/list_managers.jsp";
	public static final String PAGE_LIST_ORDERS_ADMIN = "/WEB-INF/jsp/admin/list_orders_admin.jsp";
	public static final String PAGE_ADD_MANAGER = "/WEB-INF/jsp/admin/registration_manager.jsp";
	
	// manager pages
	public static final String PAGE_LIST_RETURN_CARS = "/WEB-INF/jsp/manager/list_return_cars.jsp";
	public static final String PAGE_SHOW_DETAIL_ORDER = "/WEB-INF/jsp/manager/show_detail_order.jsp";
	public static final String PAGE_CHANGE_STATUS_CAR = "/WEB-INF/jsp/manager/change_status_car.jsp";
	public static final String PAGE_LIST_ORDERS = "/WEB-INF/jsp/manager/list_orders.jsp";
	
	// commands
	public static final String COMMAND_ADD_REPAIR = "/controller?command=addRepair";
	public static final String COMMAND_SHOW_ORDER_FOR_REPAIR = "/controller?command=showOrderForRepair";
	public static final String COMMAND_LIST_ORDERS = "/controller?command=listOrders";
	public static final String COMMAND_LIST_CAR = "/controller?command=listCar";
	public static final String COMMAND_LIST_CAR_CLASS = "/controller?command=listCarClass";
	public static final String COMMAND_LIST_USERS = "/controller?command=listUsers";
	public static final String COMMAND_LIST_MANAGERS = "/controller?command=listManagers";
	public static final String COMMAND_LIST_CAR_ADMIN = "/controller?command=listCarAdmin";
	public static final String COMMAND_LIST_RETURN_CARS = "/controller?command=listReturnCars";
	public static final String COMMAND_DELETE_ORDER = "/controller?command=deleteOrderCommand";
	public static final String COMMAND_ORDER_BASKET = "/controller?command=clientOrderBasketCommand";

}