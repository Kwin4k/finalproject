package ua.nure.korsun.SummaryTask.tests;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import ua.nure.korsun.SummaryTask.web.command.LogoutCommand;

public class MyServletTest {

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	RequestDispatcher requestDispatcher;

	@Mock
	HttpSession session;

	@Mock
	FilterChain filterChain;

	@Before
	public void setUp() throws IOException {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLogout() throws IOException {

		session.setAttribute("session", 1);
		new LogoutCommand().execute(request, response);
		assertEquals(session.getAttribute("session"), null);

	}

	@Test
	public void testLogoutReturn() throws IOException {
	//	RequestDispatcher disp = mock(RequestDispatcher.class);
	//	when(request.getRequestDispatcher("/WEB-INF/index.jsp")).thenReturn(disp);
	}

}