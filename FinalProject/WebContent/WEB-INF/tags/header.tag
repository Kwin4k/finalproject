<%@ tag language="java" pageEncoding="UTF-8"%>

<a class="nav-link disabled">${user.firstName} ${user.lastName} (${userRole.name})</a>