<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page import="ua.nure.korsun.SummaryTask.db.Role"%>

<html>

<c:set var="title" value="Users" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="my-2">
			<div class="col-md-10 order-md-3">
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="blockUser">
					<h4 class="mb-4">
						<fmt:message key="list_users_jsp.heading.users" />
					</h4>
					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message key="list_users_jsp.table.header.user_id" /></th>
								<th><fmt:message
										key="list_users_jsp.table.header.first_name" /></th>
								<th><fmt:message
										key="list_users_jsp.table.header.last_name" /></th>
								<th><fmt:message key="list_users_jsp.table.header.login" /></th>
								<th><fmt:message key="list_users_jsp.table.header.status" /></th>
								<th></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:set var="k" value="0" />
							<c:forEach var="users" items="${users}">
								<c:set var="k" value="${k+1}" />

								<tr>
									<th scope="row">${k}</th>
									<td>${users.id}</td>
									<td>${users.firstName}</td>
									<td>${users.lastName}</td>
									<td>${users.login}</td>
									<td>${usersRoleAdmin[users.roleId].getRole(users).getName()}</td>



									<c:choose>
										<c:when test="${users.roleId == 2 }">
											<td><button type="submit" name="userId"
													value="${users.id}" class="btn btn-danger btn-sm">
													<fmt:message key="list_users_jsp.button.block_user" />
												</button></td>
											<td><button type="button"
													class="btn btn-danger btn-sm" disabled>
													<fmt:message key="list_users_jsp.button.delete_user" />
												</button></td>
										</c:when>

										<c:when test="${users.roleId == 3 }">
											<td><button type="submit" name="userIdUnBlock"
													value="${users.id}" class="btn btn-primary btn-sm">
													<fmt:message key="list_users_jsp.button.unblock_user" />
												</button></td>

											<td><button type="submit" name="deleteUser"
													value="${users.id}" class="btn btn-danger btn-sm">
													<fmt:message key="list_users_jsp.button.delete_user" />
												</button></td>
										</c:when>
									</c:choose>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>