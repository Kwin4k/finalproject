<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

<c:set var="title" value="Menu" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="col-md-8 order-md-3">
			<form id="make_order" action="controller" method="post">
				<input type="hidden" name="command" value="deleteOrder">

				<table class="table">
					<thead class="thead-default">
						<tr>
							<th>#</th>
							<th>Client Id</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Car</th>
							<th>Bill</th>
							<th>Status</th>
							<th>Select</th>
						</tr>
					</thead>

					<tbody>
						<c:set var="k" value="0" />
						<c:forEach var="clientOrderBeanList" items="${clientOrderBeanList}">
							<c:set var="k" value="${k+1}" />
							<tr>
								<th scope="row">${k}</th>
								<td>${clientOrderBeanList.clientId}</td>
								<td>${clientOrderBeanList.clientFirstName}</td>
								<td>${clientOrderBeanList.clientLastName}</td>
								<td>${clientOrderBeanList.clientCar}</td>
								<td>${clientOrderBeanList.orderBill}</td>
								<td>${clientOrderBeanList.statusName}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</body>