<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Managers" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="my-2">
			<div class="col-md-11 order-md-3">
				<form id="order_form" action="controller" method="post">
					<input type="hidden" name="command" value="deleteManager" />
					<h4 class="mb-4"><fmt:message key="list_managers_jsp.heading.managers"/></h4>
					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message key="list_managers_jsp.table.header.user_id"/></th>
								<th><fmt:message key="list_managers_jsp.table.header.first_name"/></th>
								<th><fmt:message key="list_managers_jsp.table.header.last_name"/></th>
								<th><fmt:message key="list_managers_jsp.table.header.login"/></th>
								<th><fmt:message key="list_managers_jsp.table.header.status"/></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:set var="k" value="0" />
							<c:forEach var="managers" items="${managers}">
								<c:set var="k" value="${k+1}" />

								<tr>
									<th scope="row">${k}</th>
									<td>${managers.id}</td>
									<td>${managers.firstName}</td>
									<td>${managers.lastName}</td>
									<td>${managers.login}</td>
									<td><c:choose>
											<c:when test="${managers.roleId == 1 }">
												manager
											</c:when>
										</c:choose></td>
									<td><button type="submit" name="deleteManager"
											value="${managers.id}"
											class="btn btn-danger btn-sm"><fmt:message key="list_managers_jsp.button.delete_manager"/></button></td>
								</tr>
							</c:forEach>
							<tr>
								<td></td>
							</tr>
						</tbody>
					</table>
				</form>
				<form id="order_form" action="controller" method="post">
					<input type="hidden" name="command" value="viewRegistrationManager" />
					<button class="btn btn-outline-success mb-6 my-2 my-sm-0"
						type="submit"><fmt:message key="list_managers_jsp.button.registration_manager"/></button>
				</form>
			</div>
		</div>
	</div>
</body>