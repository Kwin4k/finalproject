<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<c:set var="title" value="Cars" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-9 order-md-3">
				<h4 class="h4 mb-3 font-weight-normal">
					<fmt:message key="list_car_admin_jsp.heading.list_cars" />
				</h4>

				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="changeCar">

					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message
										key="list_car_admin_jsp.table.header.car_name" /></th>
								<th><fmt:message key="list_car_admin_jsp.table.header.mark" /></th>
								<th><fmt:message
										key="list_car_admin_jsp.table.header.class" /></th>
								<th><fmt:message
										key="list_car_admin_jsp.table.header.price" /></th>
								<th><fmt:message
										key="list_car_admin_jsp.table.header.num_of_uses" /></th>
								<th><fmt:message
										key="list_car_admin_jsp.table.header.sum_for_repair" /></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:set var="k" value="0" />
							<c:forEach var="car" items="${car}">

								<c:set var="k" value="${k+1}" />
								<c:choose>
									<c:when test="${car.id != index}">
										<tr>
											<th scope="row">${k}</th>
											<td>${car.name}</td>
											<td>${car.mark}</td>
											<td>${car.carClass}</td>
											<td>${car.price}</td>
											<td>${car.numberOfUses}</td>
											<td>${car.sumForRepair}</td>
											<td><button type="submit" name="itemId"
													value="${car.id}" class="btn btn-primary btn-sm">
													<fmt:message key="list_car_admin_jsp.button.change_car" />
												</button></td>
											<td><button type="button" class="btn btn-danger btn-sm"
													disabled>
													<fmt:message key="list_car_admin_jsp.button.delete_car" />
												</button></td>
										</tr>
									</c:when>

									<c:when test="${car.id == index}">
										<tr>
											<th scope="row">${k}</th>
											<td><input type="text" class="form-control "
												name="nameCar" value="${car.name}" required=""></td>
											<td><input type="text" class="form-control"
												name="markCar" value="${car.mark}" required=""></td>
											<td><input type="text" class="form-control"
												name="carClass" value="${car.carClass}" required=""></td>
											<td><input type="text" class="form-control"
												name="carPrice" value="${car.price}" required=""></td>
											<td><button type="submit" name="carId" value="${car.id}"
													class="btn btn-primary btn-sm">
													<fmt:message key="list_car_admin_jsp.button.save_car" />
												</button></td>
											<td><button type="submit" name="deleteCarId"
													value="${car.id}" class="btn btn-danger btn-sm">
													<fmt:message key="list_car_admin_jsp.button.delete_car" />
												</button></td>
										</tr>
									</c:when>
								</c:choose>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
			<div class="col-md-3 order-md-3">
				<h4 class="mb-3">
					<fmt:message
						key="list_car_admin_jsp.selection.heading.selection_by" />
				</h4>
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="selectCar">

					<div class="col-md-10 mb-1">
						<label for="selection"><fmt:message
								key="list_car_admin_jsp.selection.label.by_class" /></label> <select
							class="custom-select d-block w-100" name="byClass">
							<option value="0"></option>
							<c:forEach var="carClass" items="${carClass}">
								<option>${carClass.carClass}</option>
							</c:forEach>
						</select> <label for="selection"><fmt:message
								key="list_car_admin_jsp.selection.label.by_mark" /></label> <select
							class="custom-select d-block w-100" name="byMark">
							<option value="0"></option>
							<c:forEach var="carMark" items="${carMark}">
								<option>${carMark.mark}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-10 mb-1">
						<button type="submit" name="select" class="btn btn-primary btn-sm">
							<fmt:message key="list_car_admin_jsp.sort.button.select" />
						</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-9 order-md-3">
			<h4 class="h4 mb-3 font-weight-normal">
				<fmt:message key="list_car_admin_jsp.heading.add_new_car" />
			</h4>
			<form id="make_order" action="controller" method="post">
				<input type="hidden" name="command" value="addNewCar">

				<table class="table">
					<thead class="thead-default">
						<tr>
							<th><fmt:message
									key="list_car_admin_jsp.table.header.car_name" /></th>
							<th><fmt:message key="list_car_admin_jsp.table.header.mark" /></th>
							<th><fmt:message key="list_car_admin_jsp.table.header.class" /></th>
							<th><fmt:message key="list_car_admin_jsp.table.header.price" /></th>
							<th></th>
						</tr>
					</thead>
					<tr>
						<td><input type="text" class="form-control" name="nameCar"
							required=""></td>
						<td><input type="text" class="form-control" name="markCar"
							required=""></td>
						<td><input type="text" class="form-control" name="carClass"
							required=""></td>
						<td><input type="text" class="form-control" name="carPrice"
							required=""></td>
						<td><button type="submit" name="carId"
								class="btn btn-primary btn-sm">
								<fmt:message key="list_car_admin_jsp.button.add_car" />
							</button></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>