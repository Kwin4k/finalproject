<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Detal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="col-md-10 order-md-3">
			<form id="make_order" action="controller" method="post">
				<input type="hidden" name="command" value="changeStatus">

				<h4 class="mb-3">
					<fmt:message key="show_detail_order_jsp.heading.order" />
				</h4>

				<table class="table">
					<thead class="thead-default">
						<tr>
							<th>#</th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.first_name" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.last_name" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.passport_id" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.email" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.address" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.phone" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.age" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.car" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.bill" /></th>
							<th><fmt:message
									key="show_detail_order_jsp.table.header.status" /></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<c:set var="k" value="1" />
						<tr>
							<th scope="row">${k}</th>
							<td>${orderBean.clientFirstName}</td>
							<td>${orderBean.clientLastName}</td>
							<td>${client.passportId}</td>
							<td>${client.email}</td>
							<td>${client.address}</td>
							<td>${client.phone}</td>
							<td>${client.age}</td>
							<td>${orderBean.clientCar}</td>
							<td>${orderBean.orderBill}</td>
							<td>${orderBean.statusName}</td>
							<c:choose>
								<c:when test="${orderBean.statusName == 'paid'}">
									<td><button class="btn btn-primary btn-sm" name="use"
											value="use" type="submit">
											<fmt:message key="show_detail_order_jsp.button.use_car" />
										</button></td>
								</c:when>
							</c:choose>
						</tr>
					</tbody>
				</table>
				<div class="form-group row">
					<c:choose>
						<c:when
							test="${orderBean.statusName == 'paid' || orderBean.statusName == 'opened'}">
							<button type="submit" value="blocked" name="blocked"
								class="btn btn-danger btn-sm">
								<fmt:message key="show_detail_order_jsp.button.block_order" />
							</button>
							<div class="col-md-9">
								<input type="text" class="form-control" name="inputComment"
									placeholder="Comment">
							</div>
						</c:when>
					</c:choose>
				</div>
			</form>
		</div>
	</div>
</body>