<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Change Status" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="col-md-10 order-md-3">
			<form id="make_order" action="controller" method="post">
				<input type="hidden" name="command" value="addRepair">
				<h4 class="h4 mb-3 font-weight-normal"><fmt:message key="change_status_car_jsp.heading.select_order"/></h4>
				<table class="table">
					<thead class="thead-default">
						<tr>
							<th>#</th>
							<th><fmt:message key="change_status_car_jsp.table.header.order_id"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.client_id"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.first_name"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.last_name"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.car"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.bill"/></th>
							<th><fmt:message key="change_status_car_jsp.table.header.status"/></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<c:set var="k" value="1" />
						<tr>
							<th scope="row">${k}</th>
							<td>${clientOrderBean.orderId}</td>
							<td>${clientOrderBean.clientId}</td>
							<td>${clientOrderBean.clientFirstName}</td>
							<td>${clientOrderBean.clientLastName}</td>
							<td>${clientOrderBean.clientCar}</td>
							<td>${clientOrderBean.orderBill}</td>
							<td>${clientOrderBean.statusName}</td>
						</tr>
					</tbody>
				</table>

				<h4 class="h4 mb-3 font-weight-normal"><fmt:message key="change_status_car_jsp.heading.add_repair_order"/></h4>

				<div class="col-md-12 order-md-3">
					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message key="change_status_car_jsp.repair.table.header.order_id"/></th>
								<th><fmt:message key="change_status_car_jsp.repair.table.header.repair_name"/></th>
								<th><fmt:message key="change_status_car_jsp.repair.table.header.repair_price"/></th>
								<th><fmt:message key="change_status_car_jsp.repair.table.header.comment"/></th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<th scope="row">${k}</th>
								<td>${clientOrderBean.orderId}</td>
								<td><input type="text" class="form-control"
									name="repairName" required=""></td>
								<td><input type="text" class="form-control"
									name="repairPrice" required=""></td>
								<td><input type="text" class="form-control" name="comment"
									required=""></td>
								<td><button type="submit" name="orderId"
										value="${clientOrderBean.orderId}"
										class="btn btn-danger btn-sm"><fmt:message key="change_status_car_jsp.button.add_repair"/></button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</body>