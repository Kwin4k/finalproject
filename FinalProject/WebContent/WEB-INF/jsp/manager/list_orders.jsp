<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page import="ua.nure.korsun.SummaryTask.db.Status"%>

<html>

<c:set var="title" value="Menu" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 order-md-3">
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="clientDetail">
					<h4 class="mb-3">
						<fmt:message key="list_orders_jsp.heading.list_orders" />
					</h4>
					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message
										key="list_orders_jsp.table.header.client_id" /></th>
								<th><fmt:message
										key="list_orders_jsp.table.header.first_name" /></th>
								<th><fmt:message
										key="list_orders_jsp.table.header.last_name" /></th>
								<th><fmt:message key="list_orders_jsp.table.header.car" /></th>
								<th><fmt:message key="list_orders_jsp.table.header.bill" /></th>
								<th><fmt:message key="list_orders_jsp.table.header.status" /></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:set var="k" value="0" />
							<c:forEach var="clientOrderBeanList"
								items="${clientOrderBeanList}">
								<c:set var="k" value="${k+1}" />
								<tr>
									<th scope="row">${k}</th>
									<td>${clientOrderBeanList.clientId}</td>
									<td>${clientOrderBeanList.clientFirstName}</td>
									<td>${clientOrderBeanList.clientLastName}</td>
									<td>${clientOrderBeanList.clientCar}</td>
									<td>${clientOrderBeanList.orderBill}</td>
									<td>${clientOrderBeanList.statusName}</td>
									<td><button type="submit" name="orderId"
											value="${clientOrderBeanList.orderId}"
											class="btn btn-primary btn-sm">
											<fmt:message key="list_orders_jsp.button.show_details" />
										</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
			<div class="col-md-1 order-md-3"></div>
			<div class="col-md-3 order-md-3">
				<h4 class="mb-3">
					<fmt:message key="list_orders_jsp.selection.heading.selection_by" />
				</h4>
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="selectOrder">

					<div class="col-md-10 mb-1">
						<label for="selection"><fmt:message
								key="list_orders_jsp.selection.labelg.by_client_id" /></label> <select
							class="custom-select d-block w-100" name="byClientId">
							<option value="null"></option>
							<c:forEach var="listClientId" items="${listClientId}">
								<option>${listClientId.clientId}</option>
							</c:forEach>
						</select> <label for="selection"><fmt:message
								key="list_orders_jsp.selection.labelg.by_status" /></label> <select
							class="custom-select d-block w-100" name="byStatus">
							<option value="null"></option>
							<c:forEach var="listStatus" items="${listStatus}">
								<option value="${listStatus.statusId}">${orderStatus[listStatus.statusId].getStatus(listStatus).getName()}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-10 mb-1">
						<button type="submit" name="select" class="btn btn-primary btn-sm">
							<fmt:message key="list_orders_jsp.selection.button.select" />
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>