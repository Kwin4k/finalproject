<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>


<html>

<c:set var="title" value="Return orders" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="col-md-11 order-md-3">
			<form id="make_order" action="controller" method="post">
				<input type="hidden" name="command" value="changeStatusToReturnCar">
					<h4 class="mb-3">
						<fmt:message key="list_return_car_jsp.heading.list_orders" />
					</h4>
				<table class="table">
					<thead class="thead-default">
						<tr>
							<th>#</th>
							<th><fmt:message key="list_return_car_jsp.table.header.client_id" /></th>
							<th><fmt:message key="list_return_car_jsp.table.header.first_name" /></th>
							<th><fmt:message key="list_return_car_jsp.table.header.last_name" /></th>
							<th><fmt:message key="list_return_car_jsp.table.header.car" /></th>
							<th><fmt:message key="list_return_car_jsp.table.header.bill" /></th>
							<th><fmt:message key="list_return_car_jsp.table.header.status" /></th>
							<th></th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<c:set var="k" value="0" />
						<c:forEach var="clientOrderBeanList" items="${clientOrderBeanList}">
							<c:set var="k" value="${k+1}" />
							<tr>
								<th scope="row">${k}</th>
								<td>${clientOrderBeanList.clientId}</td>
								<td>${clientOrderBeanList.clientFirstName}</td>
								<td>${clientOrderBeanList.clientLastName}</td>
								<td>${clientOrderBeanList.clientCar}</td>
								<td>${clientOrderBeanList.orderBill}</td>
								<td>${clientOrderBeanList.statusName}</td>
								<td><button type="submit" name="closeByOrderId" value="${clientOrderBeanList.orderId}" class="btn btn-primary btn-sm"><fmt:message key="list_return_car_jsp.button.close_order" /></button></td>
								<td><button type="submit" name="repairByOrderId" value="${clientOrderBeanList.orderId}" class="btn btn-danger btn-sm"><fmt:message key="list_return_car_jsp.button.repair_order" /></button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</body>