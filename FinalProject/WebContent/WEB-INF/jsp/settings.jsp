<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Settings" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body class="text-center">
	<div class="container">
		<form id="settings_form" action="controller" method="get">
			<input type="hidden" name="command" value="updateSettings" />
			
			<div class="my-3">
				<div class="row">
				
					<div class="col-sm-2">
						<h5 class="h5 font-weight-normal">
							<fmt:message key="settings_jsp.label.localization" />
						</h5>
					</div>
					
					<div class="mb-4">
						<select name="localeToSet">
							<c:choose>
								<c:when test="${not empty defaultLocale}">
									<option value="">${defaultLocale}[Default]</option>
								</c:when>
								<c:otherwise>
									<option value="" />
								</c:otherwise>
							</c:choose>

							<c:forEach var="localeName" items="${locales}">
								<option value="${localeName}">${localeName}</option>
							</c:forEach>
						</select>

					</div>
				</div>

				<div class="row mb-2 col-md-3">
					<label for="firstName"><fmt:message
							key="settings_jsp.label.first_name" /> </label> <input type="text"
						class="form-control" name="firstName">
				</div>

				<div class="row mb-2 col-md-3">
					<label for="lastName"><fmt:message
							key="settings_jsp.label.last_name" /> </label> <input type="text"
						class="form-control" name="lastName">
				</div>
				
				<div class="row mb-2 col-md-3">
					<label for="password"><fmt:message
							key="settings_jsp.label.password" /> </label> <input type="text"
						class="form-control" name="password">
				</div>

				<div class="row mb-2 col-md-3">
					<label for="newPassword"><fmt:message
							key="settings_jsp.label.change_password" /> </label> <input type="text"
						class="form-control" name="newPassword">
				</div>

			</div>
			<div class="col-md-3">
				<button type="submit" name="blocked" class="btn btn-danger btn-md-3">
					<fmt:message key="settings_jsp.button.update" />
				</button>
			</div>
		</form>
	</div>
</html>