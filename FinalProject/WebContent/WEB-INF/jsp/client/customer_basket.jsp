<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Basket" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="row">
			<div class="col-10">

				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="clientPayOrderCommand">

					<div class="row">

						<div class="col-md-4">
							<h3 class="mb-4">
								<fmt:message key="customer_basket_jsp.heading.enter_your_data" />
							</h3>

							<div class="mb-4">
								<label for="email"><fmt:message
										key="customer_basket_jsp.form.label.email" /> <span
									class="text-muted"></span></label> <input type="email"
									class="form-control" name="email" required
									placeholder="youmail@example.com">
							</div>

							<div class="mb-4">
								<label for="address"><fmt:message
										key="customer_basket_jsp.form.label.address" /> </label> <input
									type="text" class="form-control" name="address"
									placeholder="Выборная 99/4 кв. 237" required pattern="[а-яЁёА-Я]+.[\d]+\/[\d]+.[а-яЁёА-Я]+\..[\d]+">
							</div>

							<div class="mb-4">
								<label for="passportId"><fmt:message
										key="customer_basket_jsp.form.label.passport_id" /></label> <input
									type="text" class="form-control" name="passportId"
									placeholder="BH123456" required pattern="^[А-Я]{2}[0-9]{6}$">
							</div>

							<div class="row">
								<div class="col-md-4">
									<label for="age"><fmt:message
											key="customer_basket_jsp.form.label.age" /></label> <input
										type="number" class="form-control" name="age" required
										min="18">
								</div>
								<div class="col-md-8">
									<label for="phone"><fmt:message
											key="customer_basket_jsp.form.label.phone" /></label> <input
										type="text" class="form-control" name="phone"
										placeholder="+380" required
										pattern="^\+380\d{3}\d{2}\d{2}\d{2}$">
								</div>
							</div>
						</div>

						<div class="col-md-8 order-md-3">
							<h3 class="mb-4">
								<fmt:message key="customer_basket_jsp.heading.active_orders" />
							</h3>
							<table class="table">

								<thead class="thead-default">
									<tr>
										<th>#</th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.car_name" /></th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.price" /></th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.status" /></th>
										<th></th>
									</tr>
								</thead>

								<tbody>

									<c:set var="k" value="0" />
									<c:forEach var="basketBean" items="${basketBean}">
										<c:set var="k" value="${k+1}" />
										<tr>
											<th scope="row">${k}</th>
											<td>${basketBean.carName}</td>
											<td>${basketBean.orderBill}</td>
											<td>${basketBean.statusName}</td>
											<td><button type="submit" name="payOrder"
													value="${basketBean.orderId}"
													class="btn btn-primary btn-sm">
													<fmt:message key="customer_basket_jsp.button.pay_order" />
												</button></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

							<h3 class="mb-4">
								<fmt:message
									key="customer_basket_jsp.heading.repaired_and_blocked_orders" />
							</h3>

							<table class="table">
								<thead class="thead-default">
									<tr>
										<th>#</th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.car_name" /></th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.price" /></th>
										<th><fmt:message
												key="customer_basket_jsp.table.header.status" /></th>
										<th></th>
									</tr>
								</thead>

								<tbody>
									<c:set var="k" value="0" />
									<c:forEach var="basketBeanRepair" items="${basketBeanRepair}">
										<c:set var="k" value="${k+1}" />
										<tr>
											<th scope="row">${k}</th>
											<td>${basketBeanRepair.carName}</td>
											<td>${basketBeanRepair.orderBill}</td>
											<td>${basketBeanRepair.statusName}</td>
											<td></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>


			<div class="col-1">
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="deleteOrder">

					<div class="col-md-11">
						<h3 class="mb-4"><fmt:message key="customer_basket_jsp.header.delete" /></h3>
						<table class="table">
							<thead class="thead-default">
								<tr>
									<th><fmt:message key="customer_basket_jsp.table.header.select" /></th>
								</tr>
							</thead>

							<tbody>
								<c:forEach var="basketBean" items="${basketBean}">
									<c:set var="k" value="${k+1}" />
									<tr>
										<td><button type="submit" name="deleteOrder"
												value="${basketBean.orderId}" class="btn btn-danger btn-sm">
												<fmt:message key="customer_basket_jsp.button.delete_order" />
											</button></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>