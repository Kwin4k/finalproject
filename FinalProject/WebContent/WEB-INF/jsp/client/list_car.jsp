<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<c:set var="title" value="Cars" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 order-md-3">
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="addClientId">
					<h4 class="mb-3">
						<fmt:message key="list_car_jsp.heading.list_car" />
					</h4>
					<table class="table">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th><fmt:message key="list_car_jsp.table.header.car_name" /></th>
								<th><fmt:message key="list_car_jsp.table.header.mark" /></th>
								<th><fmt:message key="list_car_jsp.table.header.class" /></th>
								<th><fmt:message key="list_car_jsp.table.header.price" /></th>
								<th></th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<c:set var="k" value="0" />
							<c:forEach var="car" items="${car}">
								<c:set var="k" value="${k+1}" />
								<tr>
									<th scope="row">${k}</th>
									<td>${car.name}</td>
									<td>${car.mark}</td>
									<td>${car.carClass}</td>
									<td>${car.price}$</td>
									<td><button type="submit" name="itemId" value="${car.id}"
											class="btn btn-primary btn-sm">
											<fmt:message key="list_car_jsp.button.make_order" />
										</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
			<div class="col-md-1 order-md-3"></div>
			<div class="col-md-3 order-md-3">
				<h4 class="mb-3">
					<fmt:message key="list_car_jsp.sort.heading.select_sort" />
				</h4>
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="listCar">

					<div class="col-md-10 mb-1">
						<label for="sortBy"><fmt:message
								key="list_car_jsp.sort.label.sort_by" /></label> <select
							class="custom-select d-block w-100" name="sortBy">
							<option></option>
							<option value="id"><fmt:message
									key="list_car_jsp.sort.option.by_id" /></option>
							<option value="name"><fmt:message
									key="list_car_jsp.sort.option.by_name" /></option>
							<option value="class"><fmt:message
									key="list_car_jsp.sort.option.by_class" /></option>
							<option value="price"><fmt:message
									key="list_car_jsp.sort.option.by_price" /></option>
						</select>
					</div>

					<div class="col-md-10 mb-1">
						<label for="direction"><fmt:message
								key="list_car_jsp.sort.label.sort_direction" /></label> <select
							class="custom-select d-block w-100" name="direction">
							<option></option>
							<option value="down">down -> up</option>
							<option value="up">up -> down</option>
						</select>
					</div>

					<div class="col-md-10 mb-1">
						<button type="submit" name="sort" class="btn btn-primary btn-sm">
							<fmt:message key="list_car_jsp.sort.button.sort" />
						</button>
					</div>
				</form>
				<h4 class="mb-3">
					<fmt:message key="list_car_jsp.selection.heading.selection_by" />
				</h4>
				<form id="make_order" action="controller" method="post">
					<input type="hidden" name="command" value="selectCar">

					<div class="col-md-10 mb-1">
						<label for="selection"><fmt:message
								key="list_car_jsp.selection.label.by_class" /></label> <select
							class="custom-select d-block w-100" name="byClass">
							<option value="0"></option>
							<c:forEach var="carClass" items="${carClass}">
								<option>${carClass.carClass}</option>
							</c:forEach>
						</select> <label for="selection"><fmt:message
								key="list_car_jsp.selection.label.by_mark" /></label> <select
							class="custom-select d-block w-100" name="byMark">
							<option value="0"></option>
							<c:forEach var="carMark" items="${carMark}">
								<option>${carMark.mark}</option>
							</c:forEach>
						</select>

					</div>
					<div class="col-md-10 mb-1">
						<button type="submit" name="select" class="btn btn-primary btn-sm">
							<fmt:message key="list_car_jsp.selection.label.select" />
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>