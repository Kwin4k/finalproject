<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<c:set var="title" value="Order" />

<body>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="container">
		<div class="my-2">
			<div class="col-md-8 order-md-3">
				<h3 class="mb-3"><fmt:message key="client_order_jsp.heading.your_order"/></h3>
				<form id="order_form" action="controller" method="post">
					<input type="hidden" name="command" value="formClientOrderCommand" />

					<div class="form-group">
						<div class="col-md-6 mb-3">
							<h4 class="mb-3"><fmt:message key="client_order_jsp.label.select_car"/>: ${car.name}</h4>
						</div>
					</div>

					<div class="form-group row">
						<label for="example-number-input" class="col-2 col-form-label"><fmt:message key="client_order_jsp.label.days"/></label>
						<div class="col-3">
							<input class="form-control" type="number" name="days" required=""
								min="1">
						</div>
					</div>

					<h4 class="mb-3"><fmt:message key="client_order_jsp.heading.choose_services"/></h4>

					<c:forEach var="serviceList" items="${serviceList}">
						<div class="col-md-6">
							<label for="service">${serviceList.name}
								(${serviceList.price}$)</label> <input type="checkbox" name="${serviceList.id}"
								value="${serviceList.price}">
						</div>
					</c:forEach>

					<div class="col-md-6 mb-1">
						<h5 class="mb-3"><fmt:message key="client_order_jsp.label.color"/></h5>
						<select class="custom-select d-block w-100" name="color">
							<option value="Red"><fmt:message key="client_order_jsp.option.color.red"/></option>
							<option value="Blue"><fmt:message key="client_order_jsp.option.color.blue"/></option>
							<option value="Pink"><fmt:message key="client_order_jsp.option.color.pink"/></option>
							<option value="White"><fmt:message key="client_order_jsp.option.color.white"/></option>
						</select>
					</div>

					<button class="btn btn-primary btn-lg btn-block" type="submit">
						<fmt:message key="client_order_jsp.button.place_an_order"/>
					</button>
				</form>
			</div>
		</div>
	</div>
</body>