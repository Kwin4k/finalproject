<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title" value="Registration" />
<%@ include file="/WEB-INF/jspf/headLogin.jspf"%>
<%@ include file="/WEB-INF/jspf/header.jspf"%>

<body class="text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-4 order-md-3"></div>
			<div class="col-md-4 order-md-3">
				<form id="login_form" action="controller" method="post">
					<input type="hidden" name="command" value="addNewUser" />
					<div class="my-4">
						<h1 class="h3 mb-3 font-weight-normal"><fmt:message key="registration_jsp.heading.registration" /></h1>
					</div>
					<h5 class="h5 mb-3 font-weight-normal"><fmt:message key="registration_jsp.label.enter_first_name" /></h5>

					<input type="text" name="firstName" class="form-control" required=""
						autofocus="">
					<h5 class="h5 mb-3 font-weight-normal"><fmt:message key="registration_jsp.label.enter_last_name" /></h5>

					<input type="text" name="lastName" class="form-control" required=""
						autofocus="">
					<h5 class="h5 mb-3 font-weight-normal"><fmt:message key="registration_jsp.label.enter_login" /></h5>

					<input type="text" name="login" class="form-control" required=""
						autofocus="">
					<h5 class="h5 mb-3 font-weight-normal"><fmt:message key="registration_jsp.label.enter_password" /></h5>
					<input type="password" id="inputPassword" name="password"
						class="form-control" required="">
					<h5 class="h5 mb-3 font-weight-normal"><fmt:message key="registration_jsp.label.enter_password_again" /></h5>
					<input type="password" id="inputPassword" name="password2"
						class="form-control" required="">
					<div class="my-2">
						<button class="btn btn-lg btn-primary btn-block" type="submit">
							<fmt:message key="registration_jsp.button.registration" /></button>
					</div>
				</form>
			</div>
			<div class="col-md-4 order-md-3"></div>
		</div>
	</div>
</body>
</html>