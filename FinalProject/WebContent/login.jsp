<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>

<c:set var="title" value="Login" />
<%@ include file="/WEB-INF/jspf/headLogin.jspf"%>

<body class="text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-4 order-md-3"></div>
			<div class="col-md-4 order-md-3">
				<form id="login_form" action="controller" method="post">
					<input type="hidden" name="command" value="login" />
					<div class="my-5">
						<h1 class="h3 mb-3 font-weight-normal">
							<fmt:message key="login_jsp.label.sing_in" />
						</h1>
					</div>
					<h5 class="h5 mb-3 font-weight-normal">
						<fmt:message key="login_jsp.label.login" />
					</h5>

					<input type="text" name="login" class="form-control" required=""
						autofocus="">
					<h5 class="h5 mb-3 font-weight-normal">
						<fmt:message key="login_jsp.label.password" />
					</h5>
					<input type="password" id="inputPassword" name="password"
						class="form-control" required="">
					<div class="my-2">
						<button class="btn btn-lg btn-primary btn-block" type="submit">
							<fmt:message key="login_jsp.button.login" />
						</button>
					</div>
				</form>
				<div class="row">
					<div class="col-md-6">
						<form id="login_form" action="controller" method="post">
							<input type="hidden" name="command" value="viewRegistration" />
							<div class="my-1">
								<button class="btn btn-lg btn-primary btn-block" type="submit">
									<fmt:message key="login_jsp.button.registration" />
								</button>
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<form id="login_form" action="controller" method="post">
							<input type="hidden" name="command" value="viewListCar" />
							<div class="my-1">
								<button class="btn btn-lg btn-primary btn-block" type="submit">
									<fmt:message key="login_jsp.button.enter_guest" />
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4 order-md-3"></div>
		</div>
	</div>
</body>
</html>